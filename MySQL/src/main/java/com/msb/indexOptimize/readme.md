# 索引优化

## 索引基本知识

### 索引的优点

1. 大大减少了服务器需要扫描的数据量
2. 帮助服务器避免排序和建立临时表
3. 将随机 IO变成顺序 IO

### 索引的用处

1. 快速查找匹配 WHERE子句的数据行
2. 从 consideration中消除行，如果可以在多个索引间进行选择，MySQL通常会使用找到最少行的索引
3. 如果表具有多列索引，则优化器可以使用索引的任何最左前缀来查找行
4. 当有表链接的时候，从其他表检索行数据
5. 查找特定索引列的 min或 max值
6. 如果排序或分组时可用索引的最左前缀上完成的，则对表进行排序和分组
7. 在某些情况下，可以优化查询以检索值而无需查询数据行

### 索引的分类

1. 主键索引
2. 唯一索引
3. 普通索引
4. 全文索引
5. 组合索引

### 面试技术名词

1. 回表
2. 覆盖索引
3. 最左匹配
4. 索引下推

### 索引采用的数据结构

- 哈系表
- B+树

### 索引匹配方式

- 全值匹配：和索引中的所有列进行匹配

```sql
explain
select *
from staff
where name = "July"
  and age = '23'
  and pos = 'dev';
```

- 匹配最左前缀：只匹配前面几列

```sql
explain
select *
from staffs
where name = "July"
  and age = '23';
explain
select *
from staffs
where name = 'July';
```

- 匹配列前缀：匹配某一列的值的开头部分

```sql
explain
select *
from staffs
where name like "J%";
explain
select *
from staffs
where name like "%l";
```

- 匹配范围值：查找某一个范围的数据

```sql
explain
select *
from staffs
where name > 'Marry';
```

- 精准匹配某一列并范围匹配另外一列：可以查询第一列的全部和第二列的部分

```sql
explain
select *
from staff
where name = "July"
  and age > 25;
```

- 只访问索引的查询：查询时只访问索引，不访问数据行，本质上就是覆盖索引

```sql
explain
select name, age, pos
from staffs
where name = "July"
  and age = '25'
  and pos = 'dev';
```

## 哈希索引

- 基于哈希表的实现，只有精确匹配索引所有列的查询才有效
- 在 MySQL中只有 Memory的存储引擎显式的支持哈希索引
- 哈希索引自身只需存储对应的 hash值，所以索引的结构十分紧凑，这让哈希索引查找的速度非常快
- 哈希索引的限制
    1. 哈希索引只包含哈希值和行指针，而不存储字段值，索引不能使用索引中的值来避免读取行
    2. 哈希索引数据并不是按照索引值顺序排序的，所以无法进行排序
    3. 哈希索引不支持部分列匹配查找，哈希索引是使用索引的全部内容来计算哈希值
    4. 哈希索引支持等值比较查询，也不支持任何范围查询
    5. 访问哈希索引的数据非常快，除非有很多哈希冲突，当出现哈希冲突时，存储引擎必须遍历链表中的所有行指针，逐行进行比较
    6. 哈希冲突如果比较多，维护会比较麻烦

- 案例

> 当需要存储大量的 URL，并且根据 URL进行搜索查找，如果使用 B+树，存储的内容就会很大
> select id from url where url=""

> 也可以利用将url使用CRC32做哈希，可以使用以下查询方式：
> select id fom url where url="" and url_crc=CRC32("")

> 此查询性能较高原因是使用体积很小的索引来完成查找

## 组合索引

- 当包含多个列作为索引，需要注意的是正确的顺序依赖于该索引的查询，同时需要考虑如何更好的满足排序和分组的需要
- 案例

> 建立组合索引 a, b, c后，查看不同 SQL语句使用索引的情况
![](https://gitee.com/out_of_zi_wen/drawing-bed/raw/master/20210512114001.png)

## 聚簇索引和非聚簇索引

### 聚簇索引

- 不是单独的索引类型，而是一种数据存储方式
- 数据行相邻的键值紧凑的存储在一起

#### 优点

1. 可以把相关数据保存在一起
2. 数据访问更快，因为索引和数据保存在同一个树中
3. 使用覆盖索引扫描查询可以直接使用页结点中的主键值

#### 缺点

1. 聚簇索引极大的提高了 IO密集型应用的性能，如果数据全部在内存中，那么聚簇索引就没有什么优势
2. 插入速度严重依赖于插入顺序，按照主键的顺序插入是最快的方式
3. 更新聚簇索引列的代价很高，因为会强制将每个被更新的行移动到新的位置
4. 基于聚簇索引的表在插入新行，或者主键被更新导致需要移动行的时候，可能面临页分裂的问题
5. 聚簇索引可能导致全表扫描变慢，尤其是行比较稀疏，或者由于页分裂导致数据存储不连续的时候

### 非聚簇索引

数据文件跟索引文件分开存放

## 覆盖索引

### 基本介绍

1. 如果一个索引包含所有需要查询的字段的值，我们称之为覆盖索引
2. 不是所有类型的索引都可以称为覆盖索引，覆盖索引必须要存储索引列的值
3. 不同的存储实现覆盖索引的方式不同，不是所有的引擎都支持覆盖索引，Memory不支持覆盖索引

### 优势
1. 索引条目通常远小于数据行大小，如果只需要读取索引，那么 MySQL就会极大的减少数据访问量
2. 因为索引是按照列值顺序存储的，所以对于 IO密集型的范围查询会比从磁盘读取每一行数据的 IO要少得多
3. 一些存储引擎如 MyISAM在内存中只缓存索引，具体数据则由操作引擎来缓存，因此要访问数据需要一次系统调用，这可能会导致严重的性能问题
4. 由于 InnoDB的聚簇索引，覆盖索引对 InnoDB表特别有用

### 案例演示
1. 当发起一个被索引覆盖的查询时，在 explain的 extra列可以看到 using index的信息，此时就使用了覆盖索引
```sql
explain select store_id, film_id from inventory;
---
id : 1
select_type : SIMPLE
table : inventory
partition : NULL
type : index
possible_keys : NULL
key : idx_store_id_film_id
key_len : 3
ref : NULL 
rows : 4581
filtered : 100.00
extra : using index
1 row in set, 1 warning(0.01 sec)
```
2. 在大多数存储引擎中，覆盖索引只能覆盖那些只访问索引中部分列的值。不过可以进一步优化，可以使用 InnnoDB的二级索引来覆盖查询
```sql
-- actor表使用 InnoDB引擎，并在 last_name字段有二级索引，虽然该索引的列不包括主键 actor_id, 但也能够用于对 actor_id做覆盖查询
explain select actor_id, last_name from actor where last_name='HOPPER';
---
id : 1
select_type : SIMPLE
table : actor
partition : NULL 
type : ref 
possible_keys : idx_actor_last_time
key : idx_actor_last_time
key_len : 137
ref : const
rows : 2
filtered : 100.00
extra : using index
1 row in set, 1 warning (0.00 sec)
```

## 优化小细节
- 当使用索引列进行查询时，尽量不要使用表达式，把计算放到业务层而不是数据库层
```sql
select actor_name from actor where actor_id = 4;
select actor_name from actor where actor_id + 1 = 5;
```

- 尽量使用主键查询而不是其他索引，因为主键查询不会引发回表查询

- 使用前缀索引
  - 有时候需要所有很长的字符串，会让索引变得大且慢，通常情况下可以使用某个列开始的部分字符串，这样大大节约了索引空间，从而提高索引效率
  - 但是会影响索引的选择性，索引的选择性是指不重复的索引值和数据记录表记录总数的比值，范围从 1/#T 到 1
    - 索引的选择性越高则查询效率越高，可以过滤掉更多的行
  - 一般情况下，某个列前缀的选择性也是足够高的，足以满足查询的性能，但是对应 BLOB, TEXT, VARCHAR等类型的列，必须要使用前缀索引，因为 MySQL不允许索引这些列的完整长度
  - 使用该方法的诀窍在于要选择足够长的前缀以保证较高的选择性，同时又不能太长
  - 案例演示：
  
```sql
--创建数据表
create table citydemo(city varchar(50) not null);
insert into citydemo(city) select city from city;

--重复执行5次下面的sql语句
insert into citydemo(city) select city from citydemo;

--更新城市表的名称
update citydemo set city=(select city from city order by rand() limit 1);

--查找最常见的城市列表，发现每个值都出现45-65次，
select count(*) as cnt,city from citydemo group by city order by cnt desc limit 10;

--查找最频繁出现的城市前缀，先从3个前缀字母开始，发现比原来出现的次数更多，可以分别截取多个字符查看城市出现的次数
select count(*) as cnt,left(city,3) as pref from citydemo group by pref order by cnt desc limit 10;
select count(*) as cnt,left(city,7) as pref from citydemo group by pref order by cnt desc limit 10;
--此时前缀的选择性接近于完整列的选择性

--还可以通过另外一种方式来计算完整列的选择性，可以看到当前缀长度到达7之后，再增加前缀长度，选择性提升的幅度已经很小了
select count(distinct left(city,3))/count(*) as sel3,
count(distinct left(city,4))/count(*) as sel4,
count(distinct left(city,5))/count(*) as sel5,
count(distinct left(city,6))/count(*) as sel6,
count(distinct left(city,7))/count(*) as sel7,
count(distinct left(city,8))/count(*) as sel8
from citydemo;

--计算完成之后可以创建前缀索引
alter table citydemo add key(city(7));

--注意：前缀索引是一种能使索引更小更快的有效方法，但是也包含缺点：mysql无法使用前缀索引做order by 和 group by。
```
- 使用索引扫描来排序
  - MySQL有两种方式可以生成有序结果：通过排序操作或者通过索引顺序扫描，如果 explain出来的 type列的值为 index，则说明 MySQL使用了索引来做扫描
  - 扫描索引本身是很快的，因为只需要从一条索引记录移动到紧接着的下一条记录。
  - 但是如果索引不能覆盖到查询所需的全部列，那么就不得不每扫描一条索引记录就得回表查询一次对应的行，这基本都是随机 IO，因此按索引顺序读取数据的速度通常要比顺序地全表扫描慢
  - MySQL可以使用同一个索引既满足排序又用于查找行，如果可能的话，设计索引时应该尽可能地同时满足这两种任务
  - 只有当索引的列顺序和 order by子句的顺序完全一致，并且所有列的排序方式都一样时，MySQL才能使用索引来对结果进行排序；
  - 如果查询结果需要关联多张表，则当只有当 order by子句引用的字段全部为一张表时，才能使用索引做排序
  - order by子句和 查找型查询的限制是一样的，需要满足索引的最左前缀要求，否则，MySQL都需要执行顺序操作，而无法利用索引排序
  
```sql
--sakila数据库中rental表在rental_date,inventory_id,customer_id上有rental_date的索引
--使用rental_date索引为下面的查询做排序
explain select rental_id,staff_id from rental where rental_date='2005-05-25' order by inventory_id,customer_id\G
*************************** 1. row ***************************
           id: 1
  select_type: SIMPLE
        table: rental
   partitions: NULL
         type: ref
possible_keys: rental_date
          key: rental_date
      key_len: 5
          ref: const
         rows: 1
     filtered: 100.00
        Extra: Using index condition
1 row in set, 1 warning (0.00 sec)
--order by子句不满足索引的最左前缀的要求，也可以用于查询排序，这是因为所以你的第一列被指定为一个常数

--该查询为索引的第一列提供了常量条件，而使用第二列进行排序，将两个列组合在一起，就形成了索引的最左前缀
explain select rental_id,staff_id from rental where rental_date='2005-05-25' order by inventory_id desc\G
*************************** 1. row ***************************
           id: 1
  select_type: SIMPLE
        table: rental
   partitions: NULL
         type: ref
possible_keys: rental_date
          key: rental_date
      key_len: 5
          ref: const
         rows: 1
     filtered: 100.00
        Extra: Using where
1 row in set, 1 warning (0.00 sec)

--下面的查询不会利用索引
explain select rental_id,staff_id from rental where rental_date>'2005-05-25' order by rental_date,inventory_id\G
*************************** 1. row ***************************
           id: 1
  select_type: SIMPLE
        table: rental
   partitions: NULL
         type: ALL
possible_keys: rental_date
          key: NULL
      key_len: NULL
          ref: NULL
         rows: 16005
     filtered: 50.00
        Extra: Using where; Using filesort

--该查询使用了两中不同的排序方向，但是索引列都是正序排序的
explain select rental_id,staff_id from rental where rental_date>'2005-05-25' order by inventory_id desc,customer_id asc\G
*************************** 1. row ***************************
           id: 1
  select_type: SIMPLE
        table: rental
   partitions: NULL
         type: ALL
possible_keys: rental_date
          key: NULL
      key_len: NULL
          ref: NULL
         rows: 16005
     filtered: 50.00
        Extra: Using where; Using filesort
1 row in set, 1 warning (0.00 sec)

--该查询中引用了一个不再索引中的列
explain select rental_id,staff_id from rental where rental_date>'2005-05-25' order by inventory_id,staff_id\G
*************************** 1. row ***************************
           id: 1
  select_type: SIMPLE
        table: rental
   partitions: NULL
         type: ALL
possible_keys: rental_date
          key: NULL
      key_len: NULL
          ref: NULL
         rows: 16005
     filtered: 50.00
        Extra: Using where; Using filesort
1 row in set, 1 warning (0.00 sec)
```
- union all, in, or都能够使用索引，但是更推荐使用 in
```sql
explain select * from actor where actor_id = 1 union all select * from actor where actor_id = 2;
explain select * from actor where actor_id in (1, 2);
explain select * from actor where actor_id = 1 or actor_id = 2;
```

- 范围列可以用到索引
  - 范围条件是：<, >
  - 范围列可以用到索引，但是后面的列无法用到，索引最多用于一个范围列
  
- 强制类型转换会全表扫描
```sql
explain select * from user where phone = 13800001234; -- 不会触发索引
explain select * from user where phone = '13800001234'; -- 触发索引
```

- 更新十分频繁，数据区分度不高的字段上不宜建立索引
  - 更新会变更 B+数，更新频繁的字段上建立索引会大大降低数据库性能
  - 类似于性别这类区分不大的属性，建立索引是没有意义的，不能有效的过滤数据
  - 一般区分度在 80%以上的时候就可以建立索引，区分度可以使用 count(distinct('列名'))/count(*)来计算
  
- 创建索引的列，不允许为 null，可能会得到不符合预期的结果

- 当需要进行表连接的时候，最好不要超过三张表，因为需要 join的字段，数据类型必须一致

- 能使用 limit的时候尽量使用 limit

- 单表索引建议控制在 5个以内

- 创建索引时应该避免以下错误观点：
  - 索引越多越好
  - 过早优化，在不了解系统的情况下进行优化
## 索引监控
```sql
show status like 'Handler_read%';
```
- 参数解释：
  - Handler_read_first：读取索引第一个条目的次数
  - Handler_read_key：通过 index获取数据的次数
  - Handler_read_last：读取索引最后一个条目的次数
  - Handler_read_next：通过索引读取下一条数据的次数
  - Handler_read_prev：通过索引读取上一条数据的次数
  - Handler_read_rnd：从固定位置读取数据的次数
  - Handler_read_rnd_next：从数据结点读取下一条数据的次数
## 简单案例
预先准备数据
```sql
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `itdragon_order_list`;
CREATE TABLE `itdragon_order_list` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键id，默认自增长',
  `transaction_id` varchar(150) DEFAULT NULL COMMENT '交易号',
  `gross` double DEFAULT NULL COMMENT '毛收入(RMB)',
  `net` double DEFAULT NULL COMMENT '净收入(RMB)',
  `stock_id` int(11) DEFAULT NULL COMMENT '发货仓库',
  `order_status` int(11) DEFAULT NULL COMMENT '订单状态',
  `descript` varchar(255) DEFAULT NULL COMMENT '客服备注',
  `finance_descript` varchar(255) DEFAULT NULL COMMENT '财务备注',
  `create_type` varchar(100) DEFAULT NULL COMMENT '创建类型',
  `order_level` int(11) DEFAULT NULL COMMENT '订单级别',
  `input_user` varchar(20) DEFAULT NULL COMMENT '录入人',
  `input_date` varchar(20) DEFAULT NULL COMMENT '录入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10003 DEFAULT CHARSET=utf8;

INSERT INTO itdragon_order_list VALUES ('10000', '81X97310V32236260E', '6.6', '6.13', '1', '10', 'ok', 'ok', 'auto', '1', 'itdragon', '2017-08-28 17:01:49');
INSERT INTO itdragon_order_list VALUES ('10001', '61525478BB371361Q', '18.88', '18.79', '1', '10', 'ok', 'ok', 'auto', '1', 'itdragon', '2017-08-18 17:01:50');
INSERT INTO itdragon_order_list VALUES ('10002', '5RT64180WE555861V', '20.18', '20.17', '1', '10', 'ok', 'ok', 'auto', '1', 'itdragon', '2017-09-08 17:01:49');
```
**案例一**
```sql
select * from itdragon_order_list where transaction_id = "81X97310V32236260E";
--通过查看执行计划发现type=all,需要进行全表扫描
explain select * from itdragon_order_list where transaction_id = "81X97310V32236260E";

--优化一、为transaction_id创建唯一索引
 create unique index idx_order_transaID on itdragon_order_list (transaction_id);
--当创建索引之后，唯一索引对应的type是const，通过索引一次就可以找到结果，普通索引对应的type是ref，表示非唯一性索引赛秒，找到值还要进行扫描，直到将索引文件扫描完为止，显而易见，const的性能要高于ref
 explain select * from itdragon_order_list where transaction_id = "81X97310V32236260E";
 
 --优化二、使用覆盖索引，查询的结果变成 transaction_id,当extra出现using index,表示使用了覆盖索引
 explain select transaction_id from itdragon_order_list where transaction_id = "81X97310V32236260E";
```

**案例二**
```sql
--创建复合索引
create index idx_order_levelDate on itdragon_order_list (order_level,input_date);

--创建索引之后发现跟没有创建索引一样，都是全表扫描，都是文件排序
explain select * from itdragon_order_list order by order_level,input_date;

--可以使用force index强制指定索引
explain select * from itdragon_order_list force index(idx_order_levelDate) order by order_level,input_date;
--其实给订单排序意义不大，给订单级别添加索引意义也不大，因此可以先确定order_level的值，然后再给input_date排序
explain select * from itdragon_order_list where order_level=3 order by input_date;
```