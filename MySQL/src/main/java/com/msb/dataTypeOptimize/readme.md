# schema与数据类型优化

## 数据类型优化

- 更小的通常更好
    - 应该尽量使用可以正确存储数据的最小数据类型，更小的数据类型通常更快，因为它们占用更少的磁盘、内存和CPU缓存，并且处理时需要的CPU周期更少，但是要确保没有低估需要存储的值的范围，如果无法确认哪个数据类型，就选择你认为不会超过范围的最小类型
    - 案例：设计两张表，设计不同的数据类型，查看表的容量

```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class Test {
    public static void main(String[] args) throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root", "123456");
        PreparedStatement pstmt = conn.prepareStatement("insert into psn2 values(?,?)");
        for (int i = 0; i < 20000; i++) {
            pstmt.setInt(1, i);
            pstmt.setString(2, i + "");
            pstmt.addBatch();
        }
        pstmt.executeBatch();
        conn.close();
    }
}
```

- 简单就好
    - 简单数据类型的操作通常需要更少的CPU周期，例如：

        1. 整型比字符操作代价更低，因为字符集和校对规则是字符比较比整型比较更复杂，

        2. 使用 mysql自建类型而不是字符串来存储日期和时间

        3. 用整型存储IP地址
            - select INET_ATON("192.168.1.1"); // 会将 IP地址转换成整型数，内部会对 IP地址进行语法校验
            - select INET_NTOA("3232235787"); // 会将整形数转换成 IP地址

    - 案例：创建两张相同的表，改变日期的数据类型，查看SQL语句执行的速度
    
- 尽量避免 null
    - 如果查询中包含可为NULL的列，对mysql来说很难优化，因为可为null的列使得索引、索引统计和值比较都更加复杂，坦白来说，通常情况下null的列改为not null带来的性能提升比较小，所有没有必要将所有的表的schema进行修改，但是应该尽量避免设计成可为null的列
    

- 实际细则
    - 整型类型
        - 可以使用的几种整数类型：TINYINT，SMALLINT，MEDIUMINT，INT，BIGINT分别使用8，16，24，32，64位存储空间。尽量使用满足需求的最小数据类型
    - 字符和字符串类型
        - varchar根据实际内容长度保存数据，最大空间是65535个字节，适合用在长度可变的属性
            1. 使用最小的符合需求的长度
            2. varchar(n), n小于等于255使用额外一个字节保存长度, n大于255使用额外两个字节保存长度
            3. varchar(5)和 varchar(255)保存同样的内容，硬盘存储空间相同，但内存空间占用不同，是指定的大小
            4. varchar在 mysql5.6之前变更长度，或者从255以下变更到255以上时，都会导致表锁
            5. 应用场景：
                1. 存储波动较大的数据，如：文章，有的会很短，有的会很长
                2. 字符串很少更新的场景，每次更新后都会重新计算并使用额外的存储空间保证长度
                3. 适合保存多字节字符，如：汉字，特殊字符等
        - char固定长度字符串，即每条数据占用等长字节空间；最大长度是255个字符，适合用在身份证号、手机号等定长字符串
            1. 最大长度 255
            2. 会自动删除末尾的空格
            3. 检索效率、写效率会比 varchar高，以空间换时间
            4. 应用场景：
                1. 存储波动长度不大的数据，如：md5摘要
                2. 存储短字符串、经常更新的字符串
            - 实际大小和占用空间大小的关系：实际大小是使用的字节数，4K对齐后就是占用空间大小
        - BLOB和 TEXT
            - MySQL 把每个 BLOB 和 TEXT 值当作一个独立的对象处理。两者都是为了存储很大数据而设计的字符串类型，分别采用二进制和字符方式存储。
            - text不设置长度，当不知道属性的最大长度时，适合用text。按照查询速度：char>varchar>text
    - datetime和 timestamp
        - datetime
            - 占用 8个字节
            - 与时区无关，数据库底层时区配置对 datetime无效
            - 可保存到毫秒
            - 可保存时间范围大
            - 不要使用字符串存储日期类型，占用空间大，损失日期类型函数的便捷性
            - 范围在 1000-01-01到 9999-12-31
        - timestamp
            - 占用 4个字节
            - 范围: 1970-01-01到 2038-01-19
            - 精确到秒
            - 采用整形存储
            - 依赖数据库设置的时区
            - 自动更新 timestamp列的值
        - date
            - 占用的字节比使用字符串、datetime、int存储都要少，使用 date类型只需要 3个字节
            - 使用 date类型还可以利用日期时间函数进行日期之间的计算
            - date类型用于保存 1000-01-01到 9999-12-31之间的日期
        - 注意：
            1. 不要使用字符串类型来存储日期时间数据
            2. 日期时间类型通常比字符串占用的空间少
            3. 日期时间类型在进行查找过滤时可以使用日期直接比对
            4. 日期时间类型还有着丰富的处理函数，可以方便的对时间类型进行日期计算
            5. 使用 int存储日期时间不如使用 timestamp类型
    - 使用枚举代替字符串类型
        - 有时可以使用枚举类代替常用的字符串类型，mysql存储枚举类型会非常紧凑，会根据列表值的数据压缩到一个或两个字节中
        - mysql在内部会将每个值在列表中的位置保存为**整数**，并且在表的.frm文件中保存“数字-字符串”映射关系的查找表
            - create table enum_test(e enum('fish','apple','dog') not null);
            - insert into enum_test(e) values('fish'),('dog'),('apple');
            - select e+0 from enum_test;
    - 特殊类型数据
        - 人们经常使用varchar(15)来存储ip地址，然而，它的本质是32位无符号整数不是字符串，可以使用INET_ATON()和INET_NTOA函数在这两种表示方法之间转换
        - 案例：
            - select inet_aton('1.1.1.1')
            - select inet_ntoa(16843009)
    
## 合理利用范式
- 范式
    - 优点
        - 范式化的更新通常比反范式要快
        - 当数据较好的范式化后，很少或者没有重复的数据
        - 范式化后的数据比较小，可以放在内存中，操作比较快
    - 缺点
        - 通常需要表关联
    
- 反范式
    - 优点
        - 所有的数据都在同一张表中，可以避免关联
        - 可以设计有效的索引
    - 缺点
        - 表格内冗余较多，删除数据时可能会造成有用信息的丢失
    
- 注意
    - 在企业中很少能做到严格意义上的反范式或是范式，一般都是混合使用
        - 在一个网站实例中，这个网站，允许用户发送消息，并且一些用户是付费用户。现在想查看付费用户最近的10条信息。  在user表和message表中都存储用户类型(account_type)而不用完全的反范式化。这避免了完全反范式化的插入和删除问题，因为即使没有消息的时候也绝不会丢失用户的信息。这样也不会把user_message表搞得太大，有利于高效地获取数据。
        - 另一个从父表冗余一些数据到子表的理由是排序的需要。
        - 缓存衍生值也是有用的。如果需要显示每个用户发了多少消息（类似论坛的），可以每次执行一个昂贵的自查询来计算并显示它；也可以在user表中建一个num_messages列，每当用户发新消息时更新这个值。
    - 案例
        - 范式设计![](https://gitee.com/out_of_zi_wen/drawing-bed/raw/master/20210509162845.png)
        - 反范式设计![](https://gitee.com/out_of_zi_wen/drawing-bed/raw/master/20210509162904.png)
    
## 主键的选择
- 代理主键
    - 与业务无关的，无意义的数字序列
    
- 自然主键
    - 事物属性中的自然唯一标识
    
- 推荐使用代理主键
    - 不与业务耦合，更容易维护
    - 通用的键策略能够减少需要编写的源码数量，减少系统总体的拥有成本
    
## 字符集的选择
- 纯拉丁字符能表示的内容，没有必要选择 latin1以外的其他字符编码，这样能节省大量空间
- 如果我们可以确定不需要存放多种语言，就没必要非得使用 UTF8或者其他 Unicode编码
- MySQL的数据类型可以精确到字段，所以我们需要在大型数据库中存放多字节数据的时候，可以通过对不同表不同字段使用不同类型来减少存储量，进而降低 IO操作次数，提高缓存命中率

## 存储引擎的选择
![](https://gitee.com/out_of_zi_wen/drawing-bed/raw/master/20210509163717.png)

## 适当的数据冗余
- 被频繁引用且只能通过 Join 2张(或更多)大表的方式才能得到的独立小字段
- 这样的场景由于每次 Join仅仅是为了取得某个小字段的值，Join到的记录又大，会造成大量不必要的 IO，完全可以通过空间换时间的方式来优化。不过，冗余的同时需要确保数据的一致性不被破坏，确保更新的同时冗余字段也被更新

## 适当拆分
- 当我们的表中存在类似于 TEXT 或者是很大的 VARCHAR类型的大字段的时候，如果我们大部分访问这张表的时候都不需要这个字段，我们就该义无反顾的将其拆分到另外的独立表中，以减少常用数据所占用的存储空间。 
  
- 这样做的一个明显好处就是每个数据块中可以存储的数据条数可以大大增加，既减少物理 IO 次数，也能大大提高内存中的缓存命中率。
