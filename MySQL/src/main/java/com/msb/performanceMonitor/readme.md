# 性能监控

## 流程

1. Client向 Server发送请求：采用数据库连接池，减少频繁的开关连接
2. Server收到请求
    1. 连接器：控制用户的连接
    2. 分析器：词法分析，语法分析
    3. 优化器：优化 Sql语句，规定执行流程，可以查看 Sql语句的执行计划，可以采用对应的优化点，来加快查词
        - RBO：Rule Based Optimization
        - CBO：Cost Based Optimization
    4. 执行器：Sql语句的实际执行组件
        - 缓存，缓存命中率，JDK8之后已弃用

3. 存储引擎：不同的存放位置，不同的文件格式，InnoDB-磁盘，MyISAM-磁盘，Memory-内存

## 使用 show profile工具

**高版本 MySQL可能会淘汰 show profile**

### type

- all：显示所有性能信息
    - show profile all for query id

- block io：显示块 io操作的次数
    - show profile block io for query n

- context switches：显示上下文切换次数，包括主动和被动
    - show profile context switched for query n

- cpu：显示用户 cpu时间、系统 cpu时间
    - show profile cpu for query n

- IPC：显示发送和接收的消息数量
    - show profile ipc for query n

- Memory：暂未实现

- page faults：显示页错误数量
    - show profile page faults for query n

- source：显示源码中的函数名称与位置
    - show profile source for query n

- swaps：显示 swap的次数
    - show profile swap for query n

![](https://gitee.com/out_of_zi_wen/drawing-bed/raw/master/20210508202927.png)

## 使用 Performance Schema工具

> 参考 MYSQL performance schema详解.md
[](https://gitee.com/out_of_zi_wen/Architect-MSB/blob/master/MySQL/src/main/java/13%20mysql%E8%B0%83%E4%BC%98-%E7%B2%BE%E8%8B%B1%E4%B8%80%E7%8F%AD/MYSQL%20performance%20schema%E8%AF%A6%E8%A7%A3.md)

## 使用 show processlist命令查看线程个数

### 属性说明

- id：表示 session id
- user：表示用户
- host：表示操作的主机
- db：表示操作的数据库
- command：表示当前状态
    - sleep：线程正在等待客户端发送新的请求
    - query：线程正在执行查询或正在将结果发送给客户端
    - locked：在 MySql的服务层，该线程正在等待表锁
    - analyzing and statistics：线程正在收集存储引擎的统计信息，并生成查询的执行计划
    - copying to tmp table：线程正在执行查询，并且将其结果集都复制到一个临时表中
    - sorting result：线程正在对结果集进行排序
    - sending data：线程可能在多个状态之间传送数据，或者在生成结果集，或者向客户端返回数据

- info：表示详细的 sql语句
- time：表示相应命令执行时间
- state：表示命令执行状态

![](https://gitee.com/out_of_zi_wen/drawing-bed/raw/master/20210508210444.png)







  