# 查询优化

## 查询慢的原因

- 网络

- CPU

- IO

- 上下文切换

- 系统调用

- 生成统计时间

- 锁等待时间

## 优化数据访问

- 查询性能低下的主要原因是访问的数据太多，某些查询不可避免的需要筛选大量的数据，我们可以通过减少访问数据量的方式进行优化
    - 确认应用程序是否在检索大量超过需要的数据
    - 确认 MySQL服务器层是否在分析大量超过需要的数据行

- 是否向数据库请求了不需要的数据
    - 查询不需要的记录
        - 我们常常会误以为mysql会只返回需要的数据，实际上mysql却是先返回全部结果再进行计算
        - 在日常的开发习惯中，经常是先用select语句查询大量的结果，然后获取前面的N行后关闭结果集。
        - 优化方式是在查询后面添加limit

    - 多表关联时返回全部列
        - `select * from actor inner join film_actor using(actor_id) inner join film using(film_id) where
          film.title='Academy Dinosaur'`;
        - `select actor.* from actor...`;

    - 总是取出全部列
        - 在公司的企业需求中，禁止使用select *,虽然这种方式能够简化开发，但是会影响查询的性能，所以尽量不要使用

    - 重复查询相同的数据
        - 如果需要不断的重复执行相同的查询，且每次返回完全相同的数据，因此，基于这样的应用场景，我们可以将这部分数据缓存起来，这样的话能够提高查询效率

## 执行过程优化

### 查询缓存

在解析一个查询语句之前，

- 如果查询缓存是打开的，那么mysql会优先检查这个查询是否命中查询缓存中的数据

- 如果查询恰好命中了查询缓存，那么会在返回结果之前会检查用户权限

- 如果权限没有问题，那么mysql会跳过所有的阶段，就直接从缓存中拿到结果并返回给客户端

### 查询优化处理

- MySQL查询完缓存后，会执行一下几步，这几个步骤出现任何问题，都会终止查询
    1. 解析 SQL
    2. 预处理
    3. 优化 SQL执行计划

#### 语法解析器和预处理

- MySQL通过关键字将 SQL语句进行解析，并生成一颗解析树
- MySQL解析器使用 MySQL语法规则验证和解析查询，例如：
    - 验证是否使用了错误的关键字
    - 顺序是否正确

- 预处理器会进一步检查解析树是否合法，例如：
    - 表名和列名是否存在，是否有歧义
    - 验证权限信息

#### 查询优化器

```sql
select count(*)
from film_actor;
show
status like 'last_query_cost';
```

- 可以看到需要做 1104个数据页才能找到对应的数据
    - 每个表或者索引的页面个数
    - 索引的基数
    - 索引和数据行的长度
    - 索引的分布情况
- 很多情况下，MySQL可能会选择错误的执行计划，原因如下：
    1. 统计信息不准确
        - InnoDB因为其 MVCC架构，并不能维护一个数据表的行数的精确统计信息
    2. 执行计划的成本估算不等同于实际执行的成本
        - 有时候某个执行计划虽然需要读取更多的页面，但是他的成本却更小，因为如果这些页面都是顺序读或者这些页面都已经在内存中的话，那么它的访问成本将很小
        - MySQL层面并不知道哪些页面在内存中，哪些页面在磁盘中，所以查询时需要进行多少次 IO是无法得知的
    3. MySQL的最优可能和你想的不一样
        - MySQL的优化是基于成本模型的优化，但是很有可能不是最快的优化
    4. MySQL不考虑其他并发执行的优化
    5. MySQL不会考虑不受其控制的操作成本
        - 执行存储过程或者用户自定义函数的成本

- 优化器的优化策略
    - 静态优化
        - 直接对解析树进行分析，并完成优化
    - 动态优化
        - 动态优化与查询上下文有关，也可能跟取值、索引对应的行数有关
    - MySQL对查询的静态优化只有一次，但对动态优化在每次执行时都需要重新评估

- 优化器的优化类型
    - 重新定义关联表的顺序
        - 数据表的关联并不总是按照在查询中指定的顺序进行，决定关联顺序是优化器很重要的功能
    - 将外连接转化为内连接，内连接的效率高于外连接
    - 使用等价变换规则，MySQL可以使用一些等价变换规则来简化并规划表达式
    - 优化 count(), max(), min()，索引和列是否可以为空通常可以帮助 MySQL优化这类表达式
        - 如：要找到某一列的最小值，只需要查询的最左端记录即可，不需要进行全文扫描
    - 预估并转化为常数表达式
        - 当 MySQL检测到一个表达式可以转换为常数时，就会一直把该表达式当作常数来处理
    - 索引覆盖扫描
        - 当索引中的列包含所有查询中需要使用的列的时候，可以使用所有覆盖查询
    - 子查询优化
        - MySQL在某些情况下可以将子查询转换为一种效率更高的形式，从而减少多个查询多次对数据进行访问，例如将经常查询的数据放入缓存中
    - 等值传播
        - 如果两个列的值通过等式关联，那么 MySQL能够把其中一个列的 where条件传递到另一个上

```sql
explain
select film.film_id
from film
         inner join film_actor using (film_id)
where film.film_id > 500
  and film_actor.film_id > 500;
   ```

- 关联查询
    - Join的实现方式原理
        - Simple Nested-Loop Join![](https://gitee.com/out_of_zi_wen/drawing-bed/raw/master/20210516115301.png)
        - Index Nested-Loop Join![](https://gitee.com/out_of_zi_wen/drawing-bed/raw/master/20210516115400.png)
        - Block Nested-Loop Join![](https://gitee.com/out_of_zi_wen/drawing-bed/raw/master/20210516115423.png)
            1. Join Buffer会缓存所有参与查询的列而不是只有 Join的列
            2. 可以通过调整 join_buffer_size缓存大小
            3. join_buffer_size默认大小是 256K，最大值在 MySQL5.1.22版本下为 4G-1，而之后的版本在 64位机下可以申请大于 4G的空间
            4. 使用 Block Nested-Loop Join算法需要开启优化器管理配置的 optimizer_switch的设置 block_nested_loop为 on，默认为开启

    - 案例演示

```sql
-- 查看不同的顺序执行方式对查询性能的影响
explain
select film.film_id, film.title, film.release_year, actor.actor_id, actor.first_name, actor.last_name
from film
         inner join film_actor using (film_id)
         inner join actor using (actor_id);

-- 查看执行的成本
show
status like 'last_query_cost';

-- 按照自己预想的实行：
explain
select straignt_join film.film_id, film.title, film.release_year, actor.actor_id, actor.first_name, actor.last_name
from film
         inner join film_actor using (film_id)
         inner join actor using (actor_id);

-- 查看执行成本
show
status like 'last_query_cost';
```

- 排序优化
    - 无论如何，排序都是一个成本很高的操作，所以从性能的角度出发，应该尽可能避免排序或者尽可能避免对大量数据进行排序
    - 推荐使用利用索引进行排序，但是当不能使用索引进行排序时，MySQL就需要自己排序
        - 如果数据量小，则在内存中进行
        - 如果数据量大，就需要使用磁盘，称为 filesort
        - 如果需要排序的数据量小于排序缓冲区，MySQL使用内存进行快速排序操作
        - 如果内存不够排，那么 MySQL就会先将树分块，对每个独立的块使用快速排序进行排序，并将各个块的排序结果存放在磁盘上，然后将各个排好序的块进行合并，最后返回排序结果
    - 排序算法
        - 两次传输排序
            - 第一次数据读取是将需要排序的字段读取出来，然后进行排序
            - 第二次是将排好序的结果按照需要取读取数据行
            - 这种方式效率较低，原因是第二次读取时已经排好序了，需要读取所有的记录；而此时更多的是随机 IO，读取的成本会比较高
            - 优势是，在排序的时候可以存储尽可能少的数据，让排序缓冲区可以尽可能多的容纳行数来进行排序操作
        - 单次传输排序
            - 先读取查询所需要的全部列，然后根据给定列进行排序，最后返回排序结果
            - 此方式只需要一次顺序 IO读取所有的数据，而无须任何的随机 IO
            - 问题在于查询的列特别多的时候，会占用大量的存储空间，无法存储大量的数据
        - 当需要排序的列超过 max_length_for_sort_data定义的字节，MySQL会选择两次传输排序，反之使用单次，当然用户可以通过设置此参数的值来选择排序的方式

## 优化特定类型的查询

- 优化 count类型的查询
    - count()是特殊的函数，可以统计列值的数量
    - 总有人认为 MyISAM的 count函数比较快，这是有前提条件的，只在没有任何 where条件的 count(*)才是比较快的
    - 使用近似值，在某些应用场景中，不需要完全精确的值，可以参考使用近似值来代替，比如可以使用 explain来获取近似的值，或者在 OLAP应用中使用 hyperloglog算法
    - 更复杂的优化，一般情况下，count(*)需要扫描大量的行才能获取精确的数据，其实很难优化，在实际操作的时候可以考虑索引覆盖，或增加汇总表，或者增加外部缓存系统

- 优化关联查询
    - 确保 on或者 using子句中的列上有索引，在创建索引的时候，就要考虑到关联的顺序，一般在第二张表上的相应列上可以创建索引
    - 确保任何的 group by和 order by中的表达式只涉及到一个表中的列，这样 MySQL才有可能使用索引来优化这个过程

- 优化子查询
    - 子查询的最重要的优化就是尽可能使用关联语句代替

- 优化 limit分页
    - 在很多场景下，我们需要将数据进行分页，一般会使用 limit加上偏移量的方式实现，同时加上适合的 order by子句
        - 如果这种方式有索引的帮助，效率通常不错，否则的话需要进行大量的文件排序操作，但如果偏移量非常大的时候，前面的大部分数据就会被抛弃
    - 要优化这种查询，要么是在页面中限制分页的数量，要么优化大偏移量的性能，比如尽可能的使用覆盖索引而不是查询所有的列

```sql
select film_id, description
from film
order by title limt 50, 5;

explain
select film.film_id, film.description
from film
         inner join (select film_id from film order by title limit 50, 5) as lim using (film_id);
```

- 优化 union查询
    - MySQL总是通过创建并填充临时表的方式来执行 union查询，因此很多优化策略在 union查询中都没法很好的使用
        - 经常需要手工的将 where, limit, order by等子句下推到各个子查询中，以便优化器可以充分利用这些条件进行优化
    - 除非确实需要服务器消除重复的列，否则一定要使用 union all
        - 因为如果没有 all关键字，MySQL在查询的时候给临时表加上 distinct的关键字，这个操作代价很高

- 推荐使用用户自定义变量
    - 自定义变量的使用：
        - set @One:=1
        - set @min_actor:=(select min(actor_id) from actor)
        - set @last_week :=current_date-interval 1 week;
    - 自定义变量的限制：
        1. 无法使用缓存查询
        2. 不能在使用常量或者标识符的地方使用自定义变量，例如表明、列名或者 limit子句
        3. 用户自定义变量的生命周期是在一个连接中有效，所以不能用它们来做连接间的通信
        4. 不能显式的声明自定义变量的类型
        5. MySQL优化器可能在某些场景下将这些变量优化掉，导致代码不按预想的方式运行
        6. 赋值符号 :=的优先级非常低，所以需要搭配括号一起使用
        7. 使用未定义的变量不会产生任何报错
    - 自定义变量的使用案例
        - 优化排名语句：
            1. 在给一个变量赋值的同时使用这个变量：`select actor_id, @rownum:=@rownum+1 as rownum from actor limit 10;`
            2. 查询演出过电影最多的
               10名演员，并根据次数进行排序：`select actor_id, count(*) as cnt from film_actor group by actor_id order by cnt desc limit 10`
               
        - 避免重新查询刚刚更新过的数据
            - 当需要高效的更新一条记录的时间戳，同时希望查询当前记录中存放的时间戳是什么
              `update t1 set lastUpdate=now() where id=1; select lastUpdate from t1 where id=1;`
              `update t1 set lastUpdate=now() where id=1 and @now:=now(); select @now;`
              
        - 确定取值的顺序
            - 在赋值和读取变量的时候可能是在查询的不同阶段
                1. `set @rownum:=0; select actor_id, @rownum:=@rownum+1 as cnt from actor where @rownum<=1;`因为 where和 select在查询的不同阶段执行，所以只会查询到两条数据，不符合要求
                2. `set @rownum:=0; select actor_id, @rownum:=@rownum+1 as cnt from actor where @rownum<=1 order by first_name;` 当引入了 order by之后，会打印出全部的结果，因为 order by引入了文件排序，而 where条件是在排序操作前执行的
                3. `set @rownum:=0; select actor_id, @rownum as cnt from actor where (@rownum:=@rownum+1)<1;` 解决这个问题的关键在于让变量的赋值和取值发生在执行查询的同一阶段