# 执行计划
> 在企业级应用场景中，为了知道优化 SQL语句的执行，需要查看 SQL语句的具体执行过程。
> 可以使用 explain + SQL语句 的方式来模拟优化器执行 SQL语句，从而知道执行的过程
> 官网地址： https://dev.mysql.com/doc/refman/5.5/en/explain-output.html 

## 计划中包含的信息
![](https://gitee.com/out_of_zi_wen/drawing-bed/raw/master/20210510190607.png)

|    Column     |                    Meaning                     |
| :-----------: | :--------------------------------------------: |
|      id       |            The `SELECT` identifier             |
|  select_type  |               The `SELECT` type                |
|     table     |          The table for the output row          |
|  partitions   |            The matching partitions             |
|     type      |                 The join type                  |
| possible_keys |         The possible indexes to choose         |
|      key      |           The index actually chosen            |
|    key_len    |          The length of the chosen key          |
|      ref      |       The columns compared to the index        |
|     rows      |        Estimate of rows to be examined         |
|   filtered    | Percentage of rows filtered by table condition |
|     extra     |             Additional information             |

## id信息
select查询的序号，包含一组数字，表示查询中执行的 select子句或者操作表的顺序

id号分为三种情况：
1. 如果 id相同，那么执行顺序为从上到下
```sql
   explain select * from emp e join dept on e.deptno=d.deptno join salgrade sg on e.sal between sg.losal and sg.hisal;
```
2. 如果 id不同，如果是子查询，那么 id的序号会递增，id值越大优先级越高，越先被执行
```sql
   explain select * from emp e where e.deptno in (select d.deptno from dept d where d.dname = 'SALES');
```
3. 如果同时存在 id相同和不同的，那么相同的可以认为是同一组，内部从上到下执行；在组间依据优先级执行
```sql
   explain select * from emp e join dept d on e.deptno = d.deptno join salgrade sg on e.sal between sg.losal and sg.hisal where e.deptno in (select d.deptno from dept d where d.dname = 'SALES');
```

## select_type信息

主要用来分辨查询的类型，包括：普通查询、联合查询和子查询

| `select_type` Value  |                           Meaning                            |
| :------------------: | :----------------------------------------------------------: |
|        SIMPLE        |        Simple SELECT (not using UNION or subqueries)         |
|       PRIMARY        |                       Outermost SELECT                       |
|        UNION         |         Second or later SELECT statement in a UNION          |
|   DEPENDENT UNION    | Second or later SELECT statement in a UNION, dependent on outer query |
|     UNION RESULT     |                      Result of a UNION.                      |
|       SUBQUERY       |                   First SELECT in subquery                   |
|  DEPENDENT SUBQUERY  |      First SELECT in subquery, dependent on outer query      |
|       DERIVED        |                        Derived table                         |
| UNCACHEABLE SUBQUERY | A subquery for which the result cannot be cached and must be re-evaluated for each row of the outer query |
|  UNCACHEABLE UNION   | The second or later select in a UNION that belongs to an uncacheable subquery (see UNCACHEABLE SUBQUERY) |

1. SIMPLE：简单查询，不包含子查询和 UNION
```sql
explain select * from emp;
```
2. PRIMARY：查询中如果包含复杂的子查询，那么外层查询就是 PRIMARY型
```sql
explain select staname,ename supname from (select ename staname,mgr from emp) t join emp on t.mgr=emp.empno ;
```
3. UNION：第二个或者之后的 SELECT出现在 UNION之后
```sql
explain select * from emp where deptno = 10 union select * from emp where sal >2000;
```

4. DEPENDENT UNION：与 UNION类似，此处的 DEPENDENT表示 UNION或 UNION ALL联合而成的结果会受到外部表的影响
```sql
explain select * from emp e where e.empno  in ( select empno from emp where deptno = 10 union select empno from emp where sal >2000)
```
5. UNION RESULT：从 UNION表中获取结果的 SELECT
```sql
explain select * from emp where deptno = 10 union select * from emp where sal >2000;
```
6. SUBQUERY：在 SELECT或者 WHERE中包含子查询
```sql
explain select * from emp where sal > (select avg(sal) from emp) ;
```

7. DEPENDENT SUBQUERY：SUBQUERY的子查询受到外部表的影响
```sql
explain select * from emp e where e.deptno in (select distinct deptno from dept);
```

8. DERIVED：FROM子句中出现的子查询，也叫做派生类
```sql
explain select staname,ename supname from (select ename staname,mgr from emp) t join emp on t.mgr=emp.empno ;
```

9. UNCACHEABLE SUBQUERY：使用子查询的结果不能被缓存
```sql
explain select * from emp where empno = (select empno from emp where deptno=@@sort_buffer_size);
```

10. UNCACHEABLE UNION：UNION的查询结果不能被缓存

## table信息
对应行正在访问哪一个表，表名或者别名，可能是临时表或者 UNION结果集

1. 如果是具体的表名，则表示从实际的物理表中获取数据，当然也可以是表的别名
2. 如果表名为 derivedN的形式，表式采用了 id为N的查询产生的衍生表
3. 当有 union result的时候，表名是 union n1, n2等的形式，n1, n2表示参与 union的 id

## type信息

type显示的是访问类型，访问类型表示我是以何种方式去访问数据的。最容易想到的是全表扫描，直接暴力的遍历一张表去寻找需要的数据，效率低下

访问的类型有很多，效率从高到低排列：
1. system
2. const
3. eq_ref
4. ref
5. fulltext
6. ref_or_null
7. index_merge
8. unique_subquery
9. index_subquery
10. range
11. index
12. ALL

**一般情况下，需要保证查询至少达到 range级别，最好能达到 ref级别**

逐个解析：
1. all：全表扫描，一般情况下出现这种 sql语句并且数据量比较大的化，就需要进行优化
```sql
explain select * from emp;
```

2. index：全索引扫描，效率比 all好。主要有两种情况：
   1. 当前查询时，覆盖索引，即我们需要的数据在索引中即可获取
   2. 使用了索引进行排序，这样就避免了进行数据重排序
   
```sql
explain select empno from emp;
```

3. range：表示利用索引查询的时候限制了范围，在指定范围内进行查询，这样避免了 index的全索引扫描，适合用的操作符：=, >, <, <=, >=, IS NULL, BETWEEN, LIKE, or, IN等
```sql
explain select * from emp where empno between 7000 and 7500;
```

4. index_subquery：利用索引来关联子查询，不再扫描全表
```sql
explain select * from emp where emp.job in (select jobId from t_job);
```
5. unique_subquery：类似 index_subquery，使用唯一索引
```sql
explain select * from emp e where e.deptno in (select distinct deptno from dept);
```

6. index_merge：在查询过程中需要多个索引组合使用

7. ref_or_null：对于某个字段即需要关联的字段，也需要 null值的情况下，查询优化器会选择这种访问方式
```sql
explain select * from emp e where e.mgr is null or e.mgr=7696;
```

8. ref：使用了非唯一性索引进行数据查找
```sql
create index idx_3 on emp(deptno);
explain select * from emp e, dept d where e.deptno = d.deptno;
```

9. eq_ref：使用唯一性索引进行查找
```sql
expain select * from emp, emp2 where emp.empno = emp2.empno;
```
10. const：这个表至多只有一个匹配行
```sql
explain select * from emp where empno = 7639;
```

11. system：表只有一行记录，等同于系统表，是 const的特例


## possible_keys信息
显示可能应用在这张表中的索引，一个或多个，查询涉及到这个字段上的索引时，它会被列出，但不一定使用
```sql
explain select * from emp, dept where emp.deptno = dept.deptno and emp.deptno=10;
```

## key信息
实际使用的索引，如果为 null则没有使用索引，查询中若使用了覆盖索引，则该索引和查询的 select字段重叠
```sql
explain select * from emp, dept where emp.deptno = dept.deptno and emp.deptno=10;
```

## key_len信息
表示索引中使用的字节数，可以通过 key_len计算查询中使用的索引长度，在不损失精度的情况下，长度越短越好
```sql
explain select * from emp, dept where emp.deptno = dept.deptno and emp.deptno=10;
```

## rows信息
根据表的统计信息及索引使用情况，大致估算出找到所需记录需要读取的行数，此参数很重要，直接反应了当前 sql语句找了多少数据，长度越短越好
```sql
explain select * from emp;
```

## extra信息
包含额外的信息

1. using filesort：说明 mysql无法利用索引进行排序，只能利用排序算法进行排序，会消耗额外的位置
```sql
explain select * from emp order by sal;
```

2. using temporary：建立临时表来保存中间结果，查询完成之后自动将表删除
```sql
explain select ename, count(*) from emp where deptno=10 group by ename;
```

3. using index：表示当前的查询是覆盖索引的，直接从索引中读取数据，而不用访问数据表。如果同时出现 using where 表明索引被用来执行索引键值的查找，如果没有，索引被用来读取数据，而不是真正的查找
```sql
explain select deptno, count(*) from emp group by deptno limit 10;
```

4. using where：使用 where进行条件过滤
```sql
explain select * from t_user where id=1;
```

5. using join buffer：使用连接缓存

6. impossible where：where语句结果总是 false
```sql
explain select * from emp where empno = 0;
```