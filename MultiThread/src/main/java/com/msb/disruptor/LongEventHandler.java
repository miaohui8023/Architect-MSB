package com.msb.disruptor;


import com.lmax.disruptor.EventHandler;

/**
 * 定义消费者，处理容器中的元素
 */
public class LongEventHandler implements EventHandler<LongEvent> {

    public static long count = 0;

    @Override
    public void onEvent(LongEvent longEvent, long l, boolean b) throws Exception {
        count++;
        System.out.println("[" + Thread.currentThread().getName() + "]" + longEvent + " 序号：" + l);
    }
}
