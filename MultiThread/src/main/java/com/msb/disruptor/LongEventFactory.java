package com.msb.disruptor;

import com.lmax.disruptor.EventFactory;


/**
 * 定义工厂，用于填充队列
 * 调用Event工厂时，会对 ringBuffer进行内存的提前分配，GC频率会降低
 */
public class LongEventFactory implements EventFactory<LongEvent> {


    @Override
    public LongEvent newInstance() {
        return new LongEvent();
    }
}
