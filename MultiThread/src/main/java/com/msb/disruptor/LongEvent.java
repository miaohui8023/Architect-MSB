package com.msb.disruptor;

/**
 * 队伍中要处理的事件
 */
public class LongEvent {
    private long value;

    public void set(long value) {
        this.value = value;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("LongEvent{");
        sb.append("value=").append(value);
        sb.append('}');
        return sb.toString();
    }
}
