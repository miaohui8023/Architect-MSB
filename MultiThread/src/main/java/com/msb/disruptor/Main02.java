package com.msb.disruptor;

import com.lmax.disruptor.EventTranslator;
import com.lmax.disruptor.EventTranslatorOneArg;
import com.lmax.disruptor.EventTranslatorTwoArg;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.util.DaemonThreadFactory;

import java.util.concurrent.Executors;

public class Main02 {
    public static void main(String[] args) {
        // The factory for the event
        LongEventFactory factory = new LongEventFactory();

        // Specify the size of the ring buffer, must be power of 2
        int bufferSize = 1024;

        // Construct the Disruptor
        Disruptor<LongEvent> disruptor = new Disruptor<LongEvent>(factory, bufferSize, DaemonThreadFactory.INSTANCE);

        // Connect the handler
        disruptor.handleEventsWith(new LongEventHandler());

        // Start the Disruptor, starts all threads running
        disruptor.start();

        // Get the ring buffer from the Disruptor to be used for publishing
        RingBuffer<LongEvent> ringBuffer = disruptor.getRingBuffer();


        // ----------------------------------------------------- 为 Java8的 Lambda做准备
        EventTranslator<LongEvent> translator0 = new EventTranslator<LongEvent>() {
            @Override
            public void translateTo(LongEvent longEvent, long l) {
                longEvent.set(8888L);
            }
        };

        ringBuffer.publishEvent(translator0);

        // -----------------------------------------------------------
        EventTranslatorOneArg<LongEvent, Long> translator1 = new EventTranslatorOneArg<LongEvent, Long>() {
            @Override
            public void translateTo(LongEvent longEvent, long l, Long aLong) {
                longEvent.set(l);
            }
        };
        ringBuffer.publishEvent(translator1, 77777L);

        //-------------------------------------------------
        EventTranslatorTwoArg<LongEvent, Long, Long> translator2 = new EventTranslatorTwoArg<LongEvent, Long, Long>() {
            @Override
            public void translateTo(LongEvent longEvent, long l, Long aLong, Long aLong2) {
                longEvent.set(aLong + aLong2);
            }
        };

        ringBuffer.publishEvent(translator2, 1000L, 1000L);
    }
}
