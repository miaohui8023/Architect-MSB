# synchronized的底层实现

- 早期的时候，synchronized的实现是 **重量级**的，每次都需要找操作系统申请，低效
- 后来的改进：
    1. 锁升级的概念：
  > 文章：《没错，我就是厕所所长》
  > https://www.jianshu.com/p/b43b7bf5e052
  > https://www.jianshu.com/p/16c8b3707436

    2. JDK1.5之后改进了synchronized(Object)
        1. 如果只有一个线程，那么 markword记录这个线程 ID，其实没有加什么锁（偏向锁）
        2. 如果存在线程争用，就升级为自旋锁，但不会添加到等待队列（自旋锁）
        3. 自旋10次后，升级为重量级锁，去操作系统处申请更多资源（重量级锁，系统锁）
    3. 锁升级的效率并不比操作系统的 atomic原子操作慢多少，还是相当高效的
    4. 锁能升级，但是不能降级。即使后期线程数量大幅降低了，锁也依旧是升级后的锁

所以，如果线程等待时间短，等待线程少，就使用自旋锁；执行时间长，等待线程多，就是用系统锁