package com.msb.juc.c_003;

public class T1 {

    private int count = 10;

    public synchronized void m() {
        /*
        等于在方法开始前加 synchronized(this){}
         */
        count--;
        System.out.println(Thread.currentThread().getName() + " count=" + count);
    }

    public void n() {
        count++;
    }
}
