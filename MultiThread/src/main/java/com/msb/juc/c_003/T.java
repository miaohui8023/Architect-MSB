package com.msb.juc.c_003;

public class T {

    private int count = 10;

    public synchronized void m() {
        /*
        等同于在方法的代码执行时需要 synchronized(this){}
        二者的效果是一样的
         */
        count--;
        System.out.println(Thread.currentThread().getName() + " count=" + count);
    }
}
