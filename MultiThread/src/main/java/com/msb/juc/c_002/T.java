package com.msb.juc.c_002;


/**
 * synchronized
 */
public class T {

    private int count = 10;

    private void m() {
        /*
        任何线程要执行以下代码都需要先获得 this的锁
        这样就不需要每次都 new Object()作为锁对象了
         */
        synchronized (this) {
            count--;
            System.out.println(Thread.currentThread().getName() + " count=" + count);
        }
    }
}
