package com.msb.juc.c_025;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class T03_BlockingQueue_01 {
    // 使用 BlockingQueue实现
    static BlockingQueue<Character> q1 = new ArrayBlockingQueue<>(1);
    static BlockingQueue<Character> q2 = new ArrayBlockingQueue<>(1);

    public static void main(String[] args) {
        char[] aI = "123456789".toCharArray();
        char[] aC = "ABCDEFGHI".toCharArray();

        new Thread(() -> {
            for (char c : aI) {
                try {
                    System.out.print(q2.take());
                    q1.put(c);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "t1").start();

        new Thread(() -> {
            for (char c : aC) {
                try {
                    q2.put(c);
                    System.out.print(q1.take());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "t2").start();
    }
}
