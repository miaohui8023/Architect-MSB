package com.msb.juc.c_025;

import java.util.concurrent.atomic.AtomicInteger;

public class T04_AtomicInteger {
    // 使用原子类解决问题
    static AtomicInteger threadNo = new AtomicInteger(1);

    public static void main(String[] args) {
        char[] aI = "123456789".toCharArray();
        char[] aC = "ABCDEFGHI".toCharArray();

        new Thread(() -> {
            for (char c : aI) {
                while (threadNo.get() != 1) {

                }
                System.out.print(c);
                threadNo.set(0);
            }
        }, "t1").start();

        new Thread(() -> {
            for (char c : aC) {
                while (threadNo.get() != 0) {

                }
                System.out.print(c);
                threadNo.set(1);
            }
        }, "t2").start();
    }
}
