package com.msb.juc.c_000;

import java.util.concurrent.TimeUnit;

/**
 * 线程引入，什么是线程
 * program app -> QQ.exe
 * QQ running -> 进程
 * 线程 -> 一个进程里面的不同的执行路径
 * <p>
 * 纤程 -> CPU-Ring0-1 2-Ring3
 * Ring0 -> 内核态
 * Ring3 -> 用户态
 * 内核调用/系统调用 -> 线程操作
 * <p>
 * 用户启动线程：
 * 进入到内核态 -> 保存用户态的线程
 * 用户态不经过内核态的线程 -> 纤程 golang的go
 * <p>
 * 18：45
 */
public class T01_WhatIsThread {

    private static class T1 extends Thread {

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                try {
                    // Thread.sleep(1);
                    TimeUnit.MICROSECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("T1");
            }
        }
    }

    public static void main(String[] args) {
        /*
        直接调用 run()只是执行了 run()方法，并且先打印全部的 T1，再打印全部的 main
        调用 start()方法，其实创建了一条分支，T1内部方法和 main方法宏观上是并发的
            因此存在 main和 T1互相穿插的现象
         */
        // new T1().run();
        new T1().start();
        for (int i = 0; i < 10; i++) {
            try {
                TimeUnit.MICROSECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("main");
        }
    }
}
