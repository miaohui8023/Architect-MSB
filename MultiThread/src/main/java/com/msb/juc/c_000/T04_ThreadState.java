package com.msb.juc.c_000;

/**
 * 使用interrupt来控制程序流程，一般不使用
 * 在Netty源码中有
 * 在JDK锁源码中有
 * 自己写工程一般不使用它来控制业务逻辑
 * 53:11
 */
public class T04_ThreadState {

    private static class MyThread extends Thread {
        @Override
        public void run() {
            /*
                运行时状态：state:RUNNABLE
                    在RUNNABLE状态内还可以细分为：
                        Ready状态：线程被挂起或主动调用 Thread.yield()
                        Running状态：线程被调度器选中
                运行结束后，处于 Terminated状态，之后该线程就不能再start了

                还有 Blocked状态，此时程序等待进入同步代码块的锁，所得锁之后就进入了RUNNABLE状态了
                还有 Waiting状态，当调用 wait(), join(), park()等方法后就会进入，当调用 notify(), notifyAll(), unpark()等方法后就进入RUNNABLE
                还有 TimedWaiting状态，当调用 sleep(), wait(), parkNanos(), parkUntil(), join()等方法后就会进入，待时间结束后自动进入RUNNABLE

                这些状态都是 JVM管理的
             */
            System.out.println("state:" + this.getState());

            for (int i = 0; i < 10; i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println(i);
            }
        }
    }

    public static void main(String[] args) {
        Thread t = new MyThread();
        // 新建状态：stateNEW
        System.out.println("state" + t.getState());
        t.start();

        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // TERMINATED
        System.out.println(t.getState());
    }
}
