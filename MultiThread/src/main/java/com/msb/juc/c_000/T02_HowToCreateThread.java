package com.msb.juc.c_000;

/**
 * 如何创建多线程类
 * <p>
 * 一道面试题：
 * 请你告诉我启动线程的三种方式
 * 1.继承Thread
 * 2.实现Runnable
 * 3.通过线程池executors来启动
 */
public class T02_HowToCreateThread {

    /**
     * 继承 Thread类实现多线程
     */
    private static class MyThread extends Thread {
        @Override
        public void run() {
            System.out.println("Hello MyThread!~");
        }
    }


    /**
     * 实现 Runnable接口实现多线程
     */
    private static class MyRun implements Runnable {

        @Override
        public void run() {
            System.out.println("Hello MyRun!~");
        }
    }

    public static void main(String[] args) {
        // 继承Thread
        new MyThread().start();
        // 实现Runnable
        new Thread(new MyRun()).start();
        // 使用Lambda
        new Thread(() -> {
            System.out.println("Hello Lambda!~");
        }).start();
    }
}
