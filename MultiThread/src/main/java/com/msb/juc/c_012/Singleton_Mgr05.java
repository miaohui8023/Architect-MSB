package com.msb.juc.c_012;

public class Singleton_Mgr05 {
    private static Singleton_Mgr05 INSTANCE;

    private Singleton_Mgr05() {
    }

    public static Singleton_Mgr05 getInstance() {
        if (INSTANCE == null) {
            // 妄图通过同步代码块的方式提高效率，结果不可行
            synchronized (Singleton_Mgr05.class) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                INSTANCE = new Singleton_Mgr05();
            }
        }
        return INSTANCE;
    }

    public void m() {
        System.out.println("m");
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> System.out.println(Singleton_Mgr05.getInstance().hashCode())).start();
        }
    }
}
