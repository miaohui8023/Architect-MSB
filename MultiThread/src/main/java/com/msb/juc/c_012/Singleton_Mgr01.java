package com.msb.juc.c_012;

/**
 * 讲解 volatile中禁止指令重排序的 DCL单例
 * <p>
 * 饿汉式
 * 类加载到内存后，就实例化一个单例，JVM保证线程安全
 * 简单使用，推荐使用
 * 唯一缺点：不管用到与否，类装载时就完成实例化
 */
public class Singleton_Mgr01 {

    private static final Singleton_Mgr01 INSTANCE = new Singleton_Mgr01();

    private Singleton_Mgr01() {
    }


    public static Singleton_Mgr01 getInstance() {
        return INSTANCE;
    }

    public void m() {
        System.out.println("m");
    }

    public static void main(String[] args) {
        Singleton_Mgr01 m1 = Singleton_Mgr01.getInstance();
        Singleton_Mgr01 m2 = Singleton_Mgr01.getInstance();

        System.out.println(m1 == m2);
    }
}
