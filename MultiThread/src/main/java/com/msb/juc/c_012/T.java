package com.msb.juc.c_012;

import java.util.concurrent.TimeUnit;

/**
 * 第二集刚开始
 * <p>
 * volatile关键字，使一个变量在多个线程间可见
 * A B线程都用一个变量，Java默认是 A线程中保留一份 copy。这样如果 B线程修改了该变量，则 A线程未必知道
 * 而使用 volatile关键字，会让所有线程都读到变量的修改值
 * <p>
 * 在下面的代码中，running是存储在堆内存的 t对象中的
 * 当线程 t1开始运行的时候，会把 running值从内存中读到 t1线程的工作区，在运行过程中直接使用这个 copy
 * 并不会每次都去读取堆内存，这样当主线从修改 running的值后，t1线程无法感知就不会停止
 * <p>
 * 使用 volatile，将会强制所有线程都去堆内存中读取 running的值
 * <p>
 * 但是 volatile并不能包装多个线程共同修改 running变量时所带来的不一致问题，也就是说，volatile不能代替 synchronized
 * <p>
 * 参考：https://www.cnblogs.com/nexiyi/p/java_memory_model_and_thread.html
 */
public class T {
    /*
    对比一下有无 volatile的情况下，整个程序运行结果的区别
    volatile:
        保证线程之间可见
            MESI
            缓存一致性协议
            借助硬件实现
        禁止指令重排序
            DCL单例
                Double Check Lock
                Mgr06.java
            JMM，8个指令完成数据的读写，通过其中的load和store指令相互组合成的4个内存屏障实现
     */

    volatile boolean running = true;

    void m() {
        System.out.println("m start...");
        while (running) {
            /*try {
                TimeUnit.MILLISECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
        }
        System.out.println("m end ...");
    }

    public static void main(String[] args) {
        T t = new T();

        new Thread(t::m, "t1").start();

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t.running = false;
    }
}
