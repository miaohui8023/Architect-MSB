package com.msb.juc.c_012;

public class Singleton_Mgr06 {
    /*
    JIT，可以不加 volatile，但是理论上要加
    不加可能会出现的问题：
        指令重排序
    */
    private static /*volatile*/ Singleton_Mgr06 INSTANCE;

    private Singleton_Mgr06() {
    }

    public static Singleton_Mgr06 getInstance() {
        if (INSTANCE == null) {
            // 双重检查
            synchronized (T.class) {
                if (INSTANCE == null) {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    INSTANCE = new Singleton_Mgr06();
                }
            }
        }
        return INSTANCE;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> System.out.println(Singleton_Mgr06.getInstance().hashCode())).start();
        }
    }
}

