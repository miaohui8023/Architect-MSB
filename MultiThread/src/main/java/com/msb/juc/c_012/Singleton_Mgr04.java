package com.msb.juc.c_012;

/**
 * lazy loading
 * 懒汉式
 * 可以使用 synchronized关键字来解决线程不安全的问题
 * 但也带来了效率的降低
 */
public class Singleton_Mgr04 {
    private static Singleton_Mgr04 INSTANCE;

    private Singleton_Mgr04() {
    }

    public static synchronized Singleton_Mgr04 getInstance() {
        if (INSTANCE == null) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            INSTANCE = new Singleton_Mgr04();
        }
        return INSTANCE;
    }

    public void m() {
        System.out.println("m");
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> System.out.println(Singleton_Mgr04.getInstance().hashCode())).start();
        }
    }
}
