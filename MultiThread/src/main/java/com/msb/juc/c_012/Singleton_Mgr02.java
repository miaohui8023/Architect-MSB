package com.msb.juc.c_012;

/**
 * 和01是一个意思
 */
public class Singleton_Mgr02 {

    private static final Singleton_Mgr02 INSTANCE;

    static {
        INSTANCE = new Singleton_Mgr02();
    }

    private Singleton_Mgr02() {
    }

    public static Singleton_Mgr02 getInstance() {
        return INSTANCE;
    }

    public void m() {
        System.out.println("m");
    }

    public static void main(String[] args) {
        Singleton_Mgr02 m1 = Singleton_Mgr02.getInstance();
        Singleton_Mgr02 m2 = Singleton_Mgr02.getInstance();

        System.out.println(m1 == m2);
    }

}
