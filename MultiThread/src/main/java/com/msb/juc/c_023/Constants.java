package com.msb.juc.c_023;

public class Constants {
    public static final int COUNT = 100_0000;
    public static final int THREAD_COUNT = 100;
}
