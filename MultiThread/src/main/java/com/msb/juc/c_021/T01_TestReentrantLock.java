package com.msb.juc.c_021;

import java.util.concurrent.locks.ReentrantLock;

public class T01_TestReentrantLock {
    private static volatile int i = 0;

    public static void main(String[] args) {
        ReentrantLock lock = new ReentrantLock();
        // 敲个断点，进去看看！！！
        lock.lock();
        // synchronized (T01_TestReentrantLock.class) {}
        i++;
        lock.unlock();
        System.out.println(i);
    }
}
