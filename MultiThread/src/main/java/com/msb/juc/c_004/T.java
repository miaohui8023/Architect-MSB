package com.msb.juc.c_004;

public class T {

    private static int count = 10;

    public static synchronized void m() {
        /*
        这里等同于 synchronized(T.class)
        因为此时为静态方法，所以不能在使用 synchronized(this)了，
            this指代当前对象，而使用 类名.方法名()调用时，对象还未创建

        而当每个对象加载到内存后，都一定会生成对应的 Class对象，
            因此可用 类名.class的方法获取 Class对象当作锁
         */
        count--;
        System.out.println(Thread.currentThread().getName() + " count=" + count);
    }

    public static void mm() {
        /*
        这里不能使用 synchronized(this){}，理由如上
        T.class如果在同一个 ClassLoader的空间下，就一定是单例的；
            但如果是不同的加载器，那就有可能不是单例的，但不同加载器加载的对象不能混淆访问，所以没有关系
        */
        synchronized (T.class) {
            count--;
        }
    }
}
