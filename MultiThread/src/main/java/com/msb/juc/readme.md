# 内容介绍

## c_000 - c_011

- 线程的概念、启动方式、常用方法


- 线程同步
    - synchronized
        - 锁的是对象，不是代码，只有拿到锁才能访问代码
        - 普通方法上加锁 锁的是this ；静态方法上加锁，锁的是 xxx.class
        - 锁重入：锁定方法 和 非锁定方法 可以同时执行
        - 锁升级
            - 偏向锁
            - 自旋锁
            - 系统锁
        - synchronized(object)不能用 String常量、Long、int、Integer等

## c_012 - c_017

- volatile
    - 保证线程间可见
    - 禁止指令重排序
        - 单例
            - 饿汉式
            - 懒汉式（存在并发异常）
        - loadfence原语
        - storfence原语
    - volatile最好只用来修饰基本类型，如果修饰引用类型的话，就跟 final一样，是可以修改引用内部的具体内容的
- synchronized
    - 锁优化
        - 变粗粒度
        - 变细粒度
    - volatile并不能替代 synchronized

## c_018

- CAS
    - LongAdder
        - 内部是分段锁，同样也是基于 CAS
        - 当并发量特别大的时候，效率最高
    - AtomicXxx
        - 底层调用 Unsafe的 CompareAndSwap
    - increment
        - sync
        - AtomicXxx
        - LongAdder

## c_020

- ReentrantLock
    - 可以替代 synchronized
    - 底层是 CAS
    - tryLock
    - lockInterruptibly
    - 公平和非公平的切换
        - synchronized只有非公平的

- 两道面试题

## c_021

> 高并发 05集 需要再看一遍

- 源码阅读
    - 阅读原则
        - 跑不起来不读
        - 解决问题就好
        - 一条线索到底
        - 无关细节略过
        - 一般不读静态
        - 一般动态读法
    - 读源码很难
        - 理解别人的思路
            - 数据结构基础
            - 设计模式
    - 边看边画图，方法调用联系。。。

- AQS
    - 底层是 CAS和 Volatile
        - 核心是 state，被 volatile修饰的一个 int数
    - 维护着一个双向链表的队列，每个节点都是一个线程
    - Template Method
    - Callback Function
        - 父类默认实现
        - 子类具体实现

- VarHandle
    - JDK1.9之后出现
    - 创建某一个对象或者某一个东西的另一个引用，可以用作进行原子性的修改
        - 举例：int x=10; VarHandle -> x; 此时通过 x修改值不是原子性的，但是通过 VarHandle却是原子性的了
        - 创建方式：类似反射，需传入 当前对象class和 引用对象的引用名，及其class
    - CPU原语支持
    - 比反射快，直接操纵二进制码

## c_022

- ThreadLocal
    - 内容线程独有，空间换时间

- Java的四种引用：强软弱虚
  ![](https://gitee.com/out_of_zi_wen/drawing-bed/raw/master/IMG_20210402_101507.jpg)

## c_023 - c_024

- 集合容器
    - Collection
        - List
            - CopyOnWriteList
            - Vector
                - Stack
            - ArrayList
            - LinkedList
        - Set
            - HashSet
                - LinkedHashSet
            - SortedSet
                - TreeSet
            - EnumSet
            - CopyOnWriteArraySet
            - ConcurrentSkipListSet
        - Queue
            - Deque
                - ArrayDeque
                - BlockingDeque
                    - LinkedBlockingDeque
            - BlockingQueue
                - ArrayBlockingQueue
                - PriorityBlockingQueue
                - LinkedBlockingQueue
                - TransferQueue
                    - LinkedTransferQueue
                - SynchronousQueue
            - PriorityQueue
            - ConcurrentLinkedQueue
            - DelayQueue
    - Map
        - HashMap
            - LinkedHashMap
        - TreeMap
        - WeakHashMap
        - IdentityHashMap
        - ConcurrentHashMap
        - ConcurrentSkipListMap

- 思路
    - Hashtable -> ConcurrentHashMap
    - Vector -> Queue
        - 面试题：Queue和 List的区别？添加了对线程友好的 API：offer peek poll
            - BlockingQueue：put take -> 阻塞

## c_025

- 面试题
    - 两个线程交替打印 A1B2C3 ...

## c_026

- 线程池