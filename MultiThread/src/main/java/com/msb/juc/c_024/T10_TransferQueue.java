package com.msb.juc.c_024;

import java.util.concurrent.LinkedTransferQueue;

public class T10_TransferQueue {
    public static void main(String[] args) {

        /*
        装完以后阻塞等待被取走，
        如果，transfer语句在 main里面，就会阻塞整个 main线程

        可以理解为多个线程间的 SynchronousQueue
         */

        LinkedTransferQueue<String> strings = new LinkedTransferQueue<>();

        new Thread(() -> {
            try {
                strings.transfer("aaa");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();


        new Thread(() -> {
            try {
                System.out.println(strings.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();


    }
}
