package com.msb.juc.c_024;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class T06_ArrayBlockingQueue {
    static BlockingQueue<String> strings = new ArrayBlockingQueue<>(10);

    public static void main(String[] args) throws InterruptedException {
        /*
        数组实现，有容量上限
         */
        for (int i = 0; i < 10; i++) {
            // put满了就会等待，程序阻塞
            strings.put("a" + i);
        }
        strings.offer("aaa", 1, TimeUnit.SECONDS);
        System.out.println(strings);
    }
}
