package com.msb.juc.c_024;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class T05_LinkedBlockingQueue {
    static BlockingQueue<String> strings = new LinkedBlockingQueue<>();
    static Random random = new Random();

    public static void main(String[] args) {

        /*
        链表实现，无界，最大为 Integer.MAX_VALUE
        put 和 take方法实现了阻塞
         */
        new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                try {
                    // 如果满了，就会等待
                    strings.put("a" + i);
                    TimeUnit.MILLISECONDS.sleep(random.nextInt(1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "p1").start();

        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                for (; ; ) {
                    try {
                        System.out.println(Thread.currentThread().getName() + " take -" + strings.take());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }, "c" + i).start();
        }
    }
}
