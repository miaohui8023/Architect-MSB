package com.msb.juc.c_024;

import java.util.PriorityQueue;

public class T07_PriorityQueue {
    public static void main(String[] args) {

        /*
        内部是堆排序中小顶堆、大顶堆的实现
         */

        PriorityQueue<String> queue = new PriorityQueue<>();
        queue.add("c");
        queue.add("e");
        queue.add("a");
        queue.add("d");
        queue.add("z");

        for (int i = 0; i < 5; i++) {
            System.out.println(queue.poll());
        }
    }
}
