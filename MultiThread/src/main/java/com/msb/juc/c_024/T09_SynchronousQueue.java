package com.msb.juc.c_024;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;

public class T09_SynchronousQueue {
    public static void main(String[] args) throws InterruptedException {

        /*
        容量为0，不是用来装东西的，用来给线程下达任务
        类似 Exchanger

        就像两个人手对手的传递东西，如果少一个人，另一个人就得干等着

        在线程池中非常有用，线程之间进行任务调度
         */

        BlockingQueue<String> strings = new SynchronousQueue<>();

        // 创建消费者
        new Thread(() -> {
            try {
                System.out.println(strings.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        // 如果没有消费者，就会永远阻塞等待；因为容量为0，所以不能使用 add方法
        strings.put("aaa");

        System.out.println(strings.size());
    }
}
