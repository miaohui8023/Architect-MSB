package com.msb.juc.c_017;

import java.util.concurrent.TimeUnit;

/**
 * 锁定某对象 o，如果 o的熟悉发生改变，不影响锁的使用
 * 但是如果 o变成另外一个对象，则锁定的对象发生变化改变
 * 因此应该避免将锁定的对象的引用变成另外的对象
 */
public class T {

    /*final*/ Object o = new Object();

    void m() {
        synchronized (o) {
            while (true) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
            }
        }
    }

    public static void main(String[] args) {
        T t = new T();
        // 启动第一个线程
        new Thread(t::m, "t1").start();

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 创建第二个线程
        Thread t2 = new Thread(t::m, "t2");
        // 锁对象发生改变，所以 t2线程得以执行
        t.o = new Object();
        t2.start();
    }
}
