package com.msb.juc.c_020;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * CyclicBarrier循环栅栏
 * 等人满了以后，才放倒栅栏。（执行指定方法）
 */
public class T07_CyclicBarrier {

    /*
    CyclicBarrier应用场景：
        一个复杂的操作：访问数据库、访问网络、访问文件
        思路一：顺序执行，低效
        思路二：并发执行，不同的线程执行不同的操作，等所有线程全部完成后，才能执行下一步的操作
     */

    public static void main(String[] args) {

//        CyclicBarrier barrier = new CyclicBarrier(20);
//        CyclicBarrier barrier = new CyclicBarrier(20, () -> System.out.println("满人！"))

        CyclicBarrier barrier = new CyclicBarrier(20, new Runnable() {
            @Override
            public void run() {
                System.out.println("满人，发车");
            }
        });

        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                try {
                    barrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }
}
