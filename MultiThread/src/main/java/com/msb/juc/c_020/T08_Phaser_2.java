package com.msb.juc.c_020;

import java.util.Random;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

/**
 * Phaser按照不同的阶段来执行
 * 写遗传算法时可以使用
 */
public class T08_Phaser_2 {

    static Random r = new Random();
    static Phaser phaser = new MarrigePhaser();

    static void miliSleep(int milli) {
        try {
            TimeUnit.MILLISECONDS.sleep(milli);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        phaser.bulkRegister(7);

        for (int i = 0; i < 5; i++) {
            new Thread(new Person("p" + i)).start();
        }

        new Thread(new Person("新娘")).start();
        new Thread(new Person("新郎")).start();
    }

    static class MarrigePhaser extends Phaser {
        @Override
        protected boolean onAdvance(int phase, int registeredParties) {
            switch (phase) {
                case 0:
                    System.out.println("所有人都到齐了" + registeredParties);
                    return false;
                case 1:
                    System.out.println("所有人都吃完了" + registeredParties);
                    return false;
                case 2:
                    System.out.println("所有人离开了" + registeredParties);
                    return true;
                case 3:
                    System.out.println("婚礼结束，新郎新娘抱抱！" + registeredParties);
                    return true;
                default:
                    return true;
            }
        }
    }

    static class Person implements Runnable {
        String name;

        public Person(String name) {
            this.name = name;
        }

        public void arrive() {
            miliSleep(r.nextInt(1000));
            System.out.printf("%s 到达现场~\n", name);
            phaser.arriveAndAwaitAdvance();
        }

        public void eat() {
            miliSleep(r.nextInt(1000));
            System.out.printf("%s 吃完~\n", name);
            phaser.arriveAndAwaitAdvance();
        }

        public void leave() {
            miliSleep(r.nextInt(1000));
            System.out.printf("%s 离开~\n", name);
            phaser.arriveAndAwaitAdvance();
        }

        public void hug() {
            if ("新郎".equals(name) || "新娘".equals(name)) {
                miliSleep(r.nextInt(1000));
                System.out.printf("%s 洞房\n", name);
                phaser.arriveAndAwaitAdvance();
            } else {
                phaser.arriveAndDeregister();
            }
        }

        @Override
        public void run() {
            arrive();
            eat();
            leave();
            hug();
        }
    }
}


