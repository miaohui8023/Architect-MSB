package com.msb.juc.c_020;

import java.util.concurrent.TimeUnit;

/**
 * ReentrantLock用于替代synchronized
 * 本例中 m1锁定 this，只有 m1执行完毕后 m2才能执行
 */
public class T01_ReentrantLock {

    /*
    先由线程一执行方法 m1，获取了 this锁对象
    因此，线程二开启后尝试启动 m2，但是无法获得 this锁对象
    只有等到线程一全部执行完毕之后，线程二才能执行
     */

    synchronized void m1() {
        for (int i = 0; i < 10; i++) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(i);
        }
    }

    synchronized void m2() {
        System.out.println("m2 ...");
    }

    public static void main(String[] args) {
        T01_ReentrantLock rl = new T01_ReentrantLock();
        new Thread(rl::m1).start();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(rl::m2).start();
    }
}
