package com.msb.juc.c_023_01;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * 使用 Queue
 * 多线程的时候，多考虑 Queue
 */
public class T04_TicketSeller {

    static Queue<String> tickets = new ConcurrentLinkedDeque<>();

    static {
        for (int i = 0; i < 10000; i++) {
            tickets.add("票编号" + i);
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                while (true) {
                    String s = tickets.poll();
                    if (s == null) {
                        break;
                    }
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    System.out.println("销售了 -- " + s);
                }
            }).start();

        }
    }
}
