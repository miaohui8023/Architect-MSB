package com.msb.juc.c_023_01;

import java.util.ArrayList;
import java.util.List;

/**
 * 有 N张火车票，每张票对应一个编号
 * 同时有10个窗口对外售票
 * <p>
 * 使用 ArrayList
 * 会超卖
 */
public class T01_TicketSeller {
    static List<String> tickets = new ArrayList<>();

    static {
        for (int i = 0; i < 10000; i++) {
            tickets.add("票编号" + i);
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                while (tickets.size() > 0) {
                    System.out.println("销售了 -- " + tickets.remove(0));
                }
            }).start();

        }
    }
}
