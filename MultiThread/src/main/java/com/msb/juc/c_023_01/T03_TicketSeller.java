package com.msb.juc.c_023_01;

import java.util.LinkedList;
import java.util.List;

/**
 * 尽管 A操作是同步的，B操作也是同步的
 * 但是无法保证 AB的组合操作也是同步的
 */
public class T03_TicketSeller {
    static List<String> tickets = new LinkedList<>();

    static {
        for (int i = 0; i < 10000; i++) {
            tickets.add("票编号" + i);
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {

                while (true) {
                    synchronized (tickets) {
                        if (tickets.size() <= 0) {
                            break;
                        }

                        try {
                            Thread.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        System.out.println("销售了 -- " + tickets.remove(0));

                    }

                }

            }).start();

        }
    }
}
