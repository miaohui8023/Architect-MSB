package com.msb.juc.c_023_01;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * 使用 Vector
 * 依旧会超卖
 */
public class T02_TicketSeller {
    static List<String> tickets = new Vector<>();

    static {
        for (int i = 0; i < 10000; i++) {
            tickets.add("票编号" + i);
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                while (tickets.size() > 0) {

                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    System.out.println("销售了 -- " + tickets.remove(0));
                }
            }).start();

        }
    }
}
