package com.msb.juc.c_020_01;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * 使用 LockSupport来辅助通知
 */
public class T06_LockSupport {

    /*
    类似CountDownLatch，如果打印速度太快，也会出现问题，解决策略也为再加一个 LockSupport
     */
    volatile List list = new ArrayList();

    public void add(Object o) {
        list.add(o);
    }

    public int size() {
        return list.size();
    }

    public static void main(String[] args) {
        T06_LockSupport c = new T06_LockSupport();
        Thread t2 = new Thread(() -> {
            System.out.println("t2启动");
            if (c.size() != 5) {
                LockSupport.park();
            }
            System.out.println("t2 结束");
        }, "t1");
        t2.start();

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(() -> {
            System.out.println("t1 启动");
            for (int i = 0; i < 10; i++) {
                c.add(new Object());
                System.out.println("add " + i);
                if (c.size() == 5) {
                    LockSupport.unpark(t2);
                }
                /*try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
            }
        }, "t1").start();
    }
}
