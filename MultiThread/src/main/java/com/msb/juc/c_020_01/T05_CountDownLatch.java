package com.msb.juc.c_020_01;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 使用 Latch（门闩）替代 wait和 notify来进行通知
 * 好处是通信方式简单，同时也可以指定等待时间
 * 使用 await和 countDown方法替代 wait和 notify
 * 当不涉及同步，只是涉及线程通信的时候，使用 sync+wait/notify太笨重了
 * 这是应该考虑 countDownLatch/CyclicBarrier/Semaphore
 */
public class T05_CountDownLatch {
    volatile List list = new ArrayList();

    /*
    只用一个CountDownLatch会出现问题：
        当添加5个以后，打开门闩，但是因为打印的速度太快了，t2还没能继续，t1就打印下去了
    解决策略：
        再加一个 CountDownLatch
     */

    public void add(Object o) {
        list.add(o);
    }

    public int size() {
        return list.size();
    }

    public static void main(String[] args) {
        T05_CountDownLatch c = new T05_CountDownLatch();
        CountDownLatch latch = new CountDownLatch(1);
        new Thread(() -> {
            System.out.println("t2启动");
            if (c.size() != 5) {
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("t2结束");
        }, "t2").start();

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(() -> {
            System.out.println("t1启动");
            for (int i = 0; i < 10; i++) {
                c.add(new Object());
                System.out.println("add " + i);
                if (c.size() == 5) {
                    // 拉开门闩，让 t2得以执行
                    latch.countDown();
                }
                /*try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
            }
        }, "t1").start();

    }
}
