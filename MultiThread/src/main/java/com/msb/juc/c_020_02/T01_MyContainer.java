package com.msb.juc.c_020_02;


import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

/**
 * 面试题：
 * 写一个固定容量的同步容器，拥有 put和 get方法，以及 getCount方法，
 * 能够支持 2个生产者线程以及 10个消费者线程的阻塞调用
 * <p>
 * 使用 wait和 notifyAll来实现
 */
public class T01_MyContainer<T> {
    private final LinkedList<T> lists = new LinkedList<>();
    // 最多存储 10个元素
    private final int MAX = 10;
    private int count = 0;

    public synchronized void put(T t) {
        // 想想为什么用 while而不是 if
        while (lists.size() == MAX) {
            try {
                // effective java
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        lists.add(t);
        count++;
        // 通知消费者线程进行消费
        this.notifyAll();
    }

    public synchronized T get() {
        T t = null;
        while (lists.size() == 0) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        t = lists.removeFirst();
        count--;
        this.notifyAll();
        return t;
    }

    public static void main(String[] args) {
        T01_MyContainer<String> container = new T01_MyContainer<>();
        // 启动消费者线程
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                for (int j = 0; j < 5; j++) {
                    System.out.println(container.get());
                }
            }, "c" + i).start();
        }

        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 启动生产者线程
        for (int i = 0; i < 2; i++) {
            new Thread(() -> {
                for (int j = 0; j < 25; j++) {
                    container.put(Thread.currentThread().getName() + "  " + j);
                }
            }, "p" + i).start();
        }
    }
}
