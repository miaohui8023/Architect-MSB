package com.msb.juc.c_016;


import java.util.concurrent.TimeUnit;

/**
 * synchronized优化
 * 同步代码块中的语句越少越好
 * 比较 m1 和 m2
 * 细化锁的粒度、粗化锁的粒度
 */
public class FineCoarseLock {

    /*
    锁细化的情景：
    方法中有很多业务逻辑之外的不需要上锁的代码时，可以细化锁的粒度，只针对部分代码上锁

    锁粗化的情景：
    方法中写了很多的 sync代码段，锁争用很强烈，此时可以取消所有的 sync转而在方法头上加同步
     */

    int count = 0;

    synchronized void m1() {
        // do sth needn't sync
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 业务逻辑中只有下面这句代码需要 sync，这时不应该给整个方法上锁
        count++;
        // do sth needn't sync
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void m2() {
        // do sth needn't sync
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 采用细粒度的锁，可以使得线程争用事件变短，从而提高效率
        synchronized (this) {
            count++;
        }
        // do sth needn't sync
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
