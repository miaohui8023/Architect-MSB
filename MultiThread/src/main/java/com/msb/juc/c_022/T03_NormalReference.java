package com.msb.juc.c_022;

import java.io.IOException;

/**
 * 强引用
 * 普通引用就是强引用
 */
public class T03_NormalReference {
    public static void main(String[] args) throws IOException {
        M m = new M();
        m = null;

        // DisableExplicitGC
        System.gc();

        // 阻塞住当前线程
        System.in.read();
    }
}
