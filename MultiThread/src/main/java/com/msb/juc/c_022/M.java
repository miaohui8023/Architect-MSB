package com.msb.juc.c_022;

/**
 * 垃圾回收的时候，会调用 finalize方法
 */
public class M {
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("finalize");
    }
}
