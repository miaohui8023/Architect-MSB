package com.msb.juc.c_022;

import java.util.concurrent.TimeUnit;

/**
 * ThreadLocal是使用空间换时间，synchronized是使用时间换空间
 * 在Hibernate中，session就存储在 ThreadLocal中，避免 synchronized的使用
 * <p>
 * ThreadLocal内的设置的值是该线程独有的，所以 t1无法 get到 t2的set内容
 * <p>
 * set()方法内部维护了一个 TheadLocalMap，key为this即当前的ThreadLocal对象，value为设置的值
 * Map<TheadLocal, Person>
 * <p>
 * ThreadLocal用途：声明式事务，保证多个方法调用的是同一个 Connection
 */
public class T02_ThreadLocal {

    static ThreadLocal<Person> tl = new ThreadLocal<>();

    public static void main(String[] args) {
        new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(tl.get());
        }, "t2").start();

        new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            tl.set(new Person());
        }, "t1").start();
    }
}
