package com.msb.juc.c_022;

import java.lang.ref.WeakReference;

/**
 * 弱引用
 * 只要垃圾回收，就会被清除
 * <p>
 * 如果有另外一个强引用和弱引用同时指向一个对象，只要强引用消失，弱引用也消失
 * 一般用在容器中
 * <p>
 * 典型的应用就是 TheadLocal，
 * ThreadLocal 的 Map内的 Entry继承自 WeakReference<ThreadLocal<?>>
 * 所以tl通过强引用指向 ThreadLocal，而Map中的 key通过一个弱引用指向 ThreadLocal
 * 原因：
 * 如果 Entry使用强引用，那么即使 tl=null;仍有 key的引用指向 ThreadLocal对象，会有内存泄漏，而使用弱引用则不会
 * 但是还有内存泄漏，ThreadLocal被回收，key的值变为null，则导致整个 value再也无法被访问到，而ThreadLocalMap依旧存在，因此仍然存在内存泄漏
 * 所以，当用完 ThreadLocal后，一定要 remove()掉
 * ![](https://gitee.com/out_of_zi_wen/drawing-bed/raw/master/IMG_20210402_101507.jpg)
 */
public class T05_WeakReference {
    public static void main(String[] args) {
        WeakReference<M> m = new WeakReference<>(new M());

        System.out.println(m.get());
        System.gc();
        System.out.println(m.get());

        ThreadLocal<M> tl = new ThreadLocal<>();
        tl.set(new M());
        tl.remove();
    }
}
