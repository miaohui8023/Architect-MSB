package com.msb.juc.c_022;

import java.lang.ref.SoftReference;

/**
 * 软引用
 * 堆内存够的时候不会回收，内存不足时会回收
 * <p>
 * 大图片、大量数据可以使用软引用；可作缓存用
 */
public class T04_SoftReference {
    public static void main(String[] args) {
        SoftReference<byte[]> m = new SoftReference<>(new byte[1024 * 1024 * 10]);

        // m = null;
        System.out.println(m.get());
        System.gc();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(m.get());
        // 再分配一个数组，heap将装不下，这时候系统会垃圾回收，先回收一次，如果不够，会把软引用干掉
        byte[] b = new byte[1024 * 1024 * 1000];
        System.out.println(m.get());
    }
}
