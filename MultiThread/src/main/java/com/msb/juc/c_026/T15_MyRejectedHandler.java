package com.msb.juc.c_026;

import java.util.concurrent.*;

public class T15_MyRejectedHandler {

    public static void main(String[] args) {
        ExecutorService service = new ThreadPoolExecutor(4, 4, 0,
                TimeUnit.SECONDS, new ArrayBlockingQueue<>(6),
                Executors.defaultThreadFactory(), new MyHandler());
    }

    static class MyHandler implements RejectedExecutionHandler {

        @Override
        public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {


            if (threadPoolExecutor.getQueue().size() < 10000) {

            }
        }
    }
}
