package com.msb.juc.c_026;

import java.util.concurrent.*;

/**
 * Callable对 Runnable进行了扩展
 * 对 Callable的调用，可以有返回值
 */
public class T03_Callable {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Callable<String> callable = new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "hello world";
            }
        };

        ExecutorService service = Executors.newCachedThreadPool();
        // 异步提交，主线程提交之后就不管了
        Future<String> future = service.submit(callable);

        System.out.println(future.get());
        service.shutdown();
    }
}
