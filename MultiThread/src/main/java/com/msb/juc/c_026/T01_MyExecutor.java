package com.msb.juc.c_026;

import java.util.concurrent.Executor;

/**
 * 认识 Executor
 *
 */
public class T01_MyExecutor implements Executor {
    public static void main(String[] args) {
        new T01_MyExecutor().execute(() -> {
            System.out.println("hello executor");
        });
    }

    @Override
    public void execute(Runnable runnable) {
        runnable.run();
    }
}
