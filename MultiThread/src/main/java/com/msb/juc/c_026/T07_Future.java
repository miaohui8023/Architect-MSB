package com.msb.juc.c_026;

import java.util.concurrent.*;

public class T07_Future {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        /*
        FutureTask既是 Future又是 Task，既是任务又是结果
        Callable是任务
        Future是结果
         */
        FutureTask<Integer> task = new FutureTask<>(() -> {
            TimeUnit.MILLISECONDS.sleep(500);
            return 1000;
        });

        new Thread(task).start();
        // 阻塞
        System.out.println(task.get());

        // --------------------------------------
        ExecutorService service = Executors.newFixedThreadPool(5);
        Future<Integer> f = service.submit(() -> {
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return 1;
        });
        // 获取返回值
        System.out.println(f.get());
        System.out.println(f.isDone());
    }
}
