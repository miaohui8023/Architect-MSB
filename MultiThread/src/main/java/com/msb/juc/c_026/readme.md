# 线程池

## 继承体系

Executor -> ExecutorService -> ThreadPool

## Callable

Callable -> Runnable + return

## Future

Future -> 存储执行的将来才会产生的结果

## FutrureTask

FutureTask -> Future + Runnable

## ExecutorService

- submit
    - 传入 Task或者 Callable
    - 产生 Future

- invokeAll, invokeAny

- CompletableFuture
    - 底层用的是 ForkJoinPool
    - 可以用来管理多个 Future的结果

## 线程池

- ThreadPoolExecutor
    - 线程池的执行器
- ForkJoinPool
    - 分解汇总的任务
    - 用很少的线程可以执行很多的任务（子任务），TPE做不到先执行子任务
    - CPU密集型

## Executor细讲

### ExecutorService

### AbstractExecutor

### ThreadPoolExecutor

- corePoolSize
- MaximumPoolSize
- KeepAliveTime
- TimeUnit
- BlockingQueue
- ThreadFactory
- RejectPolicy
    - Abort
    - Discard
    - DiscardOldest
    - CallerRuns

### Executors

- SingleThreadExecutor
    - 为什么要有单线程的线程池？
    - 任务队列，生命周期管理
    - return new Executors.FinalizableDelegatedExecutorService(new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS,
      new LinkedBlockingQueue()));

- CachedThreadPool
    - new ThreadPoolExecutor(0, 2147483647, 60L, TimeUnit.SECONDS, new SynchronousQueue());

- FixedThreadPool
    - new ThreadPoolExecutor(nThreads, nThreads, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue());
    - 可以实现任务并行处理

- concurrent vs parallel
    - 并发：任务提交
    - 并行：任务执行，是并发的子集

- Fixed vs Cached
    - 预估并发量，《Java并发编程实战》，压力测试进行估计
    - 估算公式: Threadnum = CPUnum * CPUusage * (1 + Wait / Compute)
    - 阿里的说法：都不用，自己估算，自定义线程池

- ScheduledPool
    - super(corePoolSize, 2147483647, 10L, TimeUnit.MILLISECONDS, new ScheduledThreadPoolExecutor.DelayedWorkQueue());
    - 本质上还是 ThreadPoolExecutor
    - 定时任务线程池
    - 简单定时用 Timer，复杂定时用 Quartz

- 面试题：加入提供一个闹钟服务，订阅这个服务的人特别多，10亿，怎么优化？
    - 主服务器将任务同步到边缘服务器上，分而治之

- 以上所有线程池都是基于 ThreadPoolExecutor