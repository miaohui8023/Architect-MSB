package com.msb.juc.c_001;

/**
 * synchronized关键字
 * 对某个对象加锁
 */
public class T {

    /*
    多个线程访问同一个资源时需要上锁
    访问某一段代码，某一个临界区，临界资源时需要锁



     */

    private int count = 10;
    private Object o = new Object();

    public void m() {
        /*
          任何线程要执行以下代码，必须要先拿到 o的锁
          只有在拿到了 o锁以后，才能执行之后的代码块

        这里锁是o，被锁住的是代码段
        synchronized的底层实现，java虚拟机规范中并没有要求。

        HotSpot是在对象头上面(64位)拿出两位(mark word)来记录是否被锁定
        具体00，01，10，11表示不同的锁的类型

        锁升级
         */
        synchronized (o) {
            count--;
            System.out.println(Thread.currentThread().getName() + " count = " + count);
        }
    }


}
