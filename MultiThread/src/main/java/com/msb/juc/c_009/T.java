package com.msb.juc.c_009;

import java.util.concurrent.TimeUnit;

/**
 * 一个同步方法可以调用另外一个同步方法，
 * 一个线程已经拥有某个对象的锁，再次申请的时候仍然会得到该对象的锁
 * 也就是说，synchronized获得的锁是可重入的
 */
public class T {
    /*
        两个方法的锁都是 this
        synchronized锁是可重入的：
            当某一个线程已经获取到某个方法的锁
            并且去调用另一个该锁对应的代码块时
            能够正常访问

        原因：
            如果 synchronized锁不可重入，那么假设：
                类A为父类，有 synchronized doXxx()方法
                类B为子类，重写了 synchronized doXxx()方法，并且先调用了 super.doXxx()
                那么一旦 synchronized是不可重入的，就会死锁了，两个方法的锁都是实例化后的B对象
     */

    synchronized void m1() {
        System.out.println("m1 start ...");
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        m2();
        System.out.println("m1 end ...");
    }

    synchronized void m2() {
        System.out.println("m2 start ...");
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("m2 end ...");
    }

    public static void main(String[] args) {
        T t = new T();

        new Thread(t::m1, "t1").start();
    }
}
