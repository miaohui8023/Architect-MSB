package com.msb.builder;

public class Main {
    public static void main(String[] args) {
        TerrainBuilder builder = new ComplexTerrainBuilder();
        Terrain t = builder.buildFort().buildMine().buildFort().build();

        Person person = new Person.PersonBuilder().basicInfo(1, "zhangsan", 18)
                .loc("bj", "23").score(100).weight(60).build();
    }
}
