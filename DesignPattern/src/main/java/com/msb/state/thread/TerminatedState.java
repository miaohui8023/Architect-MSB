package com.msb.state.thread;

public class TerminatedState extends _ThreadState {

    private _Thread t;

    public TerminatedState(_Thread t) {
        this.t = t;
    }

    @Override
    void move(Action input) {

    }

    @Override
    void run() {

    }
}
