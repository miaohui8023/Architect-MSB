package com.msb.state.thread;

public class _Thread {
    _ThreadState state;

    void move(Action input) {
        state.move(input);
    }

    void run() {
        state.run();
    }
}
