package com.msb.state.thread;

public class NewState extends _ThreadState {

    private _Thread t;

    public NewState(_Thread t) {
        this.t = t;
    }

    @Override
    void move(Action input) {
        if (input.msg == "start") {
            t.state = new RunningState(t);
        }
    }

    @Override
    void run() {

    }
}
