package com.msb.state.thread;

public class RunningState extends _ThreadState {

    private _Thread t;

    public RunningState(_Thread t) {
        this.t = t;
    }

    @Override
    void move(Action input) {

    }

    @Override
    void run() {

    }
}
