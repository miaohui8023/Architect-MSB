package com.msb.state.thread;

public abstract class _ThreadState {
    abstract void move(Action input);

    abstract void run();
}
