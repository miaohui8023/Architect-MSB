package com.msb.proxy.v01;

import java.util.Random;

/**
 * 问题：想要记录坦克的移动时间
 */
public class Tank implements Movable {

    /**
     * 模拟坦克移动了一段时间
     */
    @Override
    public void move() {
        System.out.println("Tank moving ... ~");
        try {
            Thread.sleep(new Random().nextInt(10_000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

interface Movable {
    void move();
}
