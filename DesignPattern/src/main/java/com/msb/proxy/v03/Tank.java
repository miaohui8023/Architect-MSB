package com.msb.proxy.v03;

import java.util.Random;

/**
 * 使用代理记录坦克的移动时间
 */
public class Tank implements Movable {
    @Override
    public void move() {
        System.out.println("Tank moving ... ~");
        try {
            Thread.sleep(new Random().nextInt(10000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new TankTimeProxy().move();
    }
}

interface Movable {
    void move();
}

class TankTimeProxy implements Movable {

    Tank tank = new Tank();

    @Override
    public void move() {
        long start = System.currentTimeMillis();
        tank.move();
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }
}