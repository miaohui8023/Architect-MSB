package com.msb.proxy.v05;

import java.util.Random;

public class Tank implements Movable {
    @Override
    public void move() {
        System.out.println("Tank moving ... ~");
        try {
            Thread.sleep(new Random().nextInt(10000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new TankLogProxy(new TankTimeProxy(new Tank())).move();
    }
}

class TankTimeProxy implements Movable {

    Movable move;

    public TankTimeProxy(Movable move) {
        this.move = move;
    }

    @Override
    public void move() {
        long start = System.currentTimeMillis();
        move.move();
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }
}


class TankLogProxy implements Movable {

    Movable move;

    public TankLogProxy(Movable move) {
        this.move = move;
    }

    @Override
    public void move() {
        System.out.println("tank starting moving ... ~");
        move.move();
        System.out.println("tank stopped .. ~");
    }
}

interface Movable {
    void move();
}
