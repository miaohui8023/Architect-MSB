package com.msb.command;

public class InsertCommand extends Command {

    Content content;
    String strToInsert = "www.mashibing.com";

    public InsertCommand(Content content) {
        this.content = content;
    }

    @Override
    public void doIt() {
        content.msg = content.msg + strToInsert;
        System.out.println(content.msg);
    }

    @Override
    public void unDo() {
        content.msg = content.msg.substring(0, content.msg.length() - strToInsert.length());
        System.out.println(content.msg);
    }
}
