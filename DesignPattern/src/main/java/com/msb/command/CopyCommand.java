package com.msb.command;

public class CopyCommand extends Command {

    Content content;

    public CopyCommand(Content content) {
        this.content = content;
    }

    @Override
    public void doIt() {
        content.msg = content.msg + content.msg;
        System.out.println(content.msg);

    }

    @Override
    public void unDo() {
        content.msg = content.msg.substring(0, content.msg.length());
        System.out.println(content.msg);
    }
}
