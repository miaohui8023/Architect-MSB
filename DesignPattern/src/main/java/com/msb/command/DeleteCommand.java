package com.msb.command;

public class DeleteCommand extends Command {

    Content content;
    String deleted;

    public DeleteCommand(Content content) {
        this.content = content;

    }

    @Override
    public void doIt() {
        deleted = content.msg.substring(0, 5);
        content.msg = content.msg.substring(5, content.msg.length());
        System.out.println(content.msg);

    }

    @Override
    public void unDo() {
        content.msg = deleted + content.msg;
        System.out.println(content.msg);

    }
}
