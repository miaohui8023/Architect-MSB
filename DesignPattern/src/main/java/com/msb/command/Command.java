package com.msb.command;

public abstract class Command {
    public abstract void doIt();

    public abstract void unDo();
}
