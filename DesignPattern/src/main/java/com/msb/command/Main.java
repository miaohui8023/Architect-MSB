package com.msb.command;

public class Main {
    public static void main(String[] args) {
        Content c = new Content();
        InsertCommand insertCommand = new InsertCommand(c);
        insertCommand.doIt();
        insertCommand.unDo();

        CopyCommand copyCommand = new CopyCommand(c);
        copyCommand.doIt();
        copyCommand.unDo();

        DeleteCommand deleteCommand = new DeleteCommand(c);
        deleteCommand.doIt();
        deleteCommand.unDo();
    }
}
