# 备忘录模式

## 定义

保存一个对象的某个状态，以便在适当的时候恢复对象<br>
在不破坏封装性的前提下，捕获一个对象的内部状态，并在该对象之外保存这个状态

- serializable
    - 记录快照、存盘