# 设计模式列表

## 创建型模式

1. Abstract Factory
2. Builder
3. Factory Method
4. ProtoType
5. Singleton

## 结构型模式

1. Adapter
2. Bridge
3. Composite
4. Decorator
5. Facade
6. Flyweight
7. Proxy

## 行为型模式

1. Chain of Responsibility
2. Command
3. Interpreter
4. Iterator
5. Mediator
6. Memento
7. Observer
8. State
9. Strategy
10. Template Method
11. Visitor

## 面向对象编程要求

- 可维护性 Maintainability
    - 修改功能，需要改动的地方越少，可维护性就越好
- 可复用性 Reusability
    - 代码可以被以后重复使用
    - 写出自己总结的类库
- 可扩展性 Extensibility/Scalability
    - 添加功能无需修改原来的代码
- 灵活性 flexibility/mobility/adaptability
    - 代码接口可以灵活调用

## 面向对象六大原则

- 单一职责原则 Single Responsibility Principle
    - 一个类别太大，只需要负责单一的职责
    - 高内聚，低耦合
- 开闭原则 Open-Closed Principle
    - 对扩展开放，对修改关闭
    - 尽量不修改原来代码的情况下进行扩展
    - 抽象化，多态是开闭原则的关键

- 里氏替换原则 Liscov Substitution Principle
    - 所有使用父类的地方，必须能够透明的使用子类对象

- 依赖倒置原则 Dependency Inversion Principle
    - 以来抽象，而不是依赖具体
    - 面向接口编程

- 接口隔离原则 Interface Segregation Principle
    - 每个接口都应该承担独立的角色，不干不该自己干的事儿
    - Flyable Runnable 不该合二为一
    - 避免子类实现不需要实现的方法
    - 需要对客户提供接口的时候，只需要暴露最小的接口

- 迪米特法则 Low of Demeter
    - 尽量不要和陌生人说话
    - 非陌生人包括以下几类：
        - 当前对象本身
        - 以参数形式传入到当前对象方法中的对象
        - 以前对象的成员对象
        - 如果当前对象的成员对象是一个集合，那么集合中的元素也都是朋友
        - 当前对象所创建的对象
    - 降低与其他类的耦合度