package com.msb.iterator.v3;


public class _LinkedList implements _Collection {

    Node head = null;
    Node tail = null;

    private int size = 0;

    @Override
    public void add(Object object) {
        Node n = new Node(object);
        n.next = null;

        if (head == null) {
            head = n;
            tail = n;
        }

        tail.next = n;
        tail = n;

        size++;
    }

    @Override
    public int size() {
        return size;
    }

    private class Node {
        private Object msg;
        Node next;

        public Node(Object msg) {
            this.msg = msg;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Node{");
            sb.append("msg=").append(msg);
            sb.append(", next=").append(next);
            sb.append('}');
            return sb.toString();
        }
    }
}
