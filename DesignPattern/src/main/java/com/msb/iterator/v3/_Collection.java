package com.msb.iterator.v3;

public interface _Collection {
    void add(Object o);

    int size();
}
