package com.msb.iterator.v6;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * 对比 java.util下的 ArrayList和 Collection
 */
public class Main {
    public static void main(String[] args) {
        Collection list = new ArrayList();
        for (int i = 0; i < 15; i++) {
            list.add(new String("s" + i));
        }

        Iterator it = list.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
