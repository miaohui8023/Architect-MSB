package com.msb.iterator.v1;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        _ArrayList list = new _ArrayList();
        for (int i = 0; i < 15; i++) {
            list.add(new String("s" + i));
        }
        System.out.println(list);
    }
}


/**
 * 相比于数组，这个容器可以不考虑边界问题，可以动态扩展
 */
class _ArrayList {
    Object[] objects = new Object[10];
    private int index = 0;

    public void add(Object o) {
        if (index == objects.length) {
            Object[] newObjects = new Object[objects.length * 2];
            System.arraycopy(objects, 0, newObjects, 0, objects.length);
            objects = newObjects;
        }

        objects[index++] = o;
    }

    public int size() {
        return index;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("_ArrayList{");
        sb.append("objects=").append(objects == null ? "null" : Arrays.asList(objects).toString());
        sb.append(", index=").append(index);
        sb.append('}');
        return sb.toString();
    }
}
