package com.msb.iterator.v4;

public interface _Collection {
    void add(Object o);

    int size();
}
