package com.msb.iterator.v4;

/**
 * 如何对容器进行遍历
 */
public class Main {
    public static void main(String[] args) {
        _Collection list = new _ArrayList();
        for (int i = 0; i < 15; i++) {
            list.add(new String("s" + i));
        }
        System.out.println(list.size());

        _ArrayList al = (_ArrayList) list;
        for (int i = 0; i < al.size(); i++) {
            // 如果采用这种方式遍历，就不能实现通用了
            // 解决措施：每种具体的实现，定义自己的遍历方式
        }
    }
}
