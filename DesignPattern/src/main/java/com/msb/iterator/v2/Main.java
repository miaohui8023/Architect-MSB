package com.msb.iterator.v2;

public class Main {
    public static void main(String[] args) {
        _LinkedList list = new _LinkedList();
        for (int i = 0; i < 15; i++) {
            list.add(new String("l" + i));
        }

        System.out.println(list);
    }
}

class _LinkedList {
    Node head = null;
    Node tail = null;

    private int size = 0;

    public void add(Object object) {
        Node n = new Node(object);
        n.next = null;

        if (head == null) {
            head = n;
            tail = n;
        }

        tail.next = n;
        tail = n;

        size++;
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("_LinkedList{");
        sb.append("head=").append(head);
        sb.append(", tail=").append(tail);
        sb.append(", size=").append(size);
        sb.append('}');
        return sb.toString();
    }
}

class Node {
    private Object msg;
    Node next;

    public Node(Object msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Node{");
        sb.append("msg=").append(msg);
        sb.append(", next=").append(next);
        sb.append('}');
        return sb.toString();
    }
}