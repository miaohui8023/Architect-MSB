package com.msb.iterator.v7;

public class Main {
    public static void main(String[] args) {
        _Collection<String> list = new _ArrayList<>();
        for (int i = 0; i < 15; i++) {
            list.add(new String("s" + i));
        }
        System.out.println(list.size());

        _Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            String o = iterator.next();
            System.out.println(o);
        }
    }
}
