package com.msb.iterator.v7;

public interface _Iterator<T> {
    boolean hasNext();

    T next();
}
