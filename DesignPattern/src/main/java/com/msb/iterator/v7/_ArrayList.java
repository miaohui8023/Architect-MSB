package com.msb.iterator.v7;

public class _ArrayList<T> implements _Collection<T> {

    T[] objects = (T[]) new Object[10];
    private int index = 0;

    @Override
    public void add(T t) {
        if (index == objects.length) {
            T[] newObjects = (T[]) new Object[objects.length * 2];
            System.arraycopy(objects, 0, newObjects, 0, objects.length);
            objects = newObjects;
        }

        objects[index++] = t;
    }

    @Override
    public int size() {
        return index;
    }

    @Override
    public _Iterator<T> iterator() {
        return new ArrayListIterator<T>();
    }

    private class ArrayListIterator<T> implements _Iterator<T> {

        private int currentIndex = 0;

        @Override
        public boolean hasNext() {
            if (currentIndex >= index) {
                return false;
            }
            return true;
        }

        @Override
        public T next() {
            T t = (T) objects[currentIndex];
            currentIndex++;
            return t;
        }
    }
}
