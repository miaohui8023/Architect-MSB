package com.msb.iterator.v7;

public interface _Collection<T> {
    void add(T t);
    int size();

    _Iterator<T> iterator();
}
