package com.msb.iterator.v5;

public class Main {
    public static void main(String[] args) {
        _Collection list = new _ArrayList();
        for (int i = 0; i < 15; i++) {
            list.add(new String("s" + i));
        }
        System.out.println(list.size());

        // 这个接口的调用方式
        _Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            Object o = iterator.next();
            System.out.println(o);
        }
    }
}
