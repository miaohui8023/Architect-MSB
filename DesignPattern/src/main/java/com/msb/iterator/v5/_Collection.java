package com.msb.iterator.v5;

public interface _Collection {
    void add(Object o);

    int size();

    _Iterator iterator();
}
