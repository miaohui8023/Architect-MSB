package com.msb.iterator.v5;


/**
 * 自定义迭代器接口
 */
public interface _Iterator {

    boolean hasNext();

    Object next();
}
