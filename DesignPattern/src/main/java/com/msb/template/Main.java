package com.msb.template;

public class Main {
    public static void main(String[] args) {
        Father father = new Son1();
        father.m();
    }
}

abstract class Father {
    void m() {
        op1();
        op2();
    }

    abstract void op1();

    abstract void op2();
}


class Son1 extends Father {

    @Override
    void op1() {
        System.out.println("op1");
    }

    @Override
    void op2() {
        System.out.println("op2");
    }
}
