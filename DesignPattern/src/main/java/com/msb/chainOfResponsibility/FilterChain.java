package com.msb.chainOfResponsibility;

import java.util.ArrayList;
import java.util.List;

public class FilterChain implements Filter {
    List<Filter> filters = new ArrayList<>();

    public FilterChain add(Filter filter) {
        filters.add(filter);
        return this;
    }

    public void del(Filter filter) {
        filters.remove(filter);
    }

    @Override
    public boolean doFilter(Message message) {
        for (Filter filter : filters) {
            if (!filter.doFilter(message)) {
                return false;
            }
        }
        return true;
    }
}
