package com.msb.chainOfResponsibility;

public interface Filter {
    boolean doFilter(Message message);
}
