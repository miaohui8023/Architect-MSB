package com.msb.chainOfResponsibility;

public class SensitiveFilter implements Filter {
    @Override
    public boolean doFilter(Message message) {
        String r = message.getMsg();
        r = r.replaceAll("sb", "**");
        message.setMsg(r);
        return false;
    }
}
