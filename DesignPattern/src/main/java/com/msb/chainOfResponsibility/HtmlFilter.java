package com.msb.chainOfResponsibility;

public class HtmlFilter implements Filter {
    @Override
    public boolean doFilter(Message message) {
        String r = message.getMsg();
        r = r.replaceAll("<", "[");
        r = r.replaceAll(">", "]");
        message.setMsg(r);
        return true;
    }
}
