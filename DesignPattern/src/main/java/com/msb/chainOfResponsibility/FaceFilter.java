package com.msb.chainOfResponsibility;

public class FaceFilter implements Filter {
    @Override
    public boolean doFilter(Message message) {
        String r = message.getMsg();
        r = r.replaceAll(":\\)", "^v^");
        message.setMsg(r);
        return true;
    }
}
