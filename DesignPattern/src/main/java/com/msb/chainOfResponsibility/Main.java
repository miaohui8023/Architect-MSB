package com.msb.chainOfResponsibility;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Message message = new Message();
        message.setMsg("大家好:),<script>, 欢迎访问 msb，");

        // 处理 message
        String r = message.getMsg();

        /*r = r.replaceAll("<", "[");
        r = r.replaceAll(">", "]");
        r = r.replaceAll("sb", "**");*/

        /*List<Filter> filters = new ArrayList<>();
        filters.add(new SensitiveFilter());
        filters.add(new HtmlFilter());

        for (Filter filter : filters) {
            filter.doFilter(message);
        }*/

        FilterChain chain = new FilterChain();
        chain.add(new SensitiveFilter()).add(new HtmlFilter());


        FilterChain chain1 = new FilterChain();
        chain1.add(new FaceFilter());

        chain.add(chain1);
        chain.doFilter(message);

        System.out.println(message.getMsg());
    }
}
