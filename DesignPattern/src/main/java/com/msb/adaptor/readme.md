# 适配器模式

## 定义

将现有的模块接口转换一下，以适配新的接口需求，用于填补“现有的程序“和“所需的程序“之间差异<br>

举例：

- jdbc-odbc bridge
- ASM transformer
- 不同IO流的创建

误区：
- 很多常见的 XxxAdapter类反而不是 Adapter模式
    - WindowAdapter
    - KeyAdapter
    - 都不是 适配器