package com.msb.singleton;

/**
 * 懒汉式
 * 试图减小同步代码块的范围，来提高效率，但并不可行
 * 多个线程间在判断 INSTANCE为空后，因为 synchronized的关系会各自串行初始化对象，相当于没有加锁
 */
public class Mgr05 {
    private static Mgr05 INSTANCE;

    private Mgr05() {
    }

    public static Mgr05 getInstance() {
        if (INSTANCE == null) {
            synchronized (Mgr05.class) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                INSTANCE = new Mgr05();
            }
        }
        return INSTANCE;
    }

    public void m() {
        System.out.println("m");
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                // 尽管已经加了 synchronized关键字，但是 hashcode值仍不相同
                System.out.println(Mgr05.getInstance().hashCode());
            }).start();
        }
    }
}
