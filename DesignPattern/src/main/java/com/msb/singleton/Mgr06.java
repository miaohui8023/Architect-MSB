package com.msb.singleton;


/**
 * 懒汉式
 * 双重检查，判断 INSTANCE是否为 null
 * Double Check Lock, DCL
 * 但是 因为重排序使得 将INSTANCE引用指向了仅分配完内存但还未初始化的对象，其他线程可能会访问到一个初始化未完全的对象
 * 所以可以加一个 volatile
 */
public class Mgr06 {
    private static volatile Mgr06 INSTANCE;

    private Mgr06() {
    }

    public static Mgr06 getInstance() {
        // 第一次检查 INSTANCE是否为 null
        if (INSTANCE == null) {
            synchronized (Mgr06.class) {
                // 第二次检查 INSTANCE是否为 null
                if (INSTANCE == null) {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    INSTANCE = new Mgr06();
                }
            }
        }
        return INSTANCE;
    }

    public void m() {
        System.out.println("m");
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                System.out.println(Mgr06.getInstance().hashCode());
            }).start();
        }
    }
}
