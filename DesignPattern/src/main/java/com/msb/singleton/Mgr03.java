package com.msb.singleton;


/**
 * 懒加载
 * 虽然达到了按需初始化的目的，但是存在线程不安全的问题
 */
public class Mgr03 {
    private static Mgr03 INSTANCE;

    private Mgr03() {
    }

    public static Mgr03 getInstance() {
        if (INSTANCE == null) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            INSTANCE = new Mgr03();
        }
        return INSTANCE;
    }

    public void m() {
        System.out.println("m");
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                // 存在某几个对象的 hashcode值不相同，线程不安全
                System.out.println(Mgr03.getInstance().hashCode());
            }).start();
        }
    }
}
