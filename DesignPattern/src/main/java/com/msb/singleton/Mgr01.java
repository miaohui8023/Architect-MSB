package com.msb.singleton;

/**
 * 饿汉式
 * 类加载到内存后，就实例化一个单例对象，
 * JVM保证纤程安全，每个 class只会 load到内存一次，因此 INSTANCE一定只有一个
 * 简单实用，推荐使用！
 * 缺点：不管是否用到，类装载时都会完成实例化
 * <p>
 * 私有构造方法
 * 只对外开放 getInstance()方法返回单例实例
 */
public class Mgr01 {
    private static final Mgr01 INSTANCE = new Mgr01();

    private Mgr01() {
    }

    public static Mgr01 getInstance() {
        return INSTANCE;
    }

    public void m() {
        System.out.println("m");
    }

    public static void main(String[] args) {
        Mgr01 m1 = Mgr01.getInstance();
        Mgr01 m2 = Mgr01.getInstance();
        // true，m1和 m2是同一个对象
        System.out.println(m1 == m2);

    }
}
