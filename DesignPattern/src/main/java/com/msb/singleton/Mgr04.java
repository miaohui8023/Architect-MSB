package com.msb.singleton;


/**
 * 懒汉式
 * 给 getInstance()方法加上 synchronized
 * 但是效率会变得很低，因为不管 INSTANCE是否已经被实例化了，都需要先获取锁再调用 getInstance()
 */
public class Mgr04 {
    private static Mgr04 INSTANCE;

    private Mgr04() {
    }

    // 锁定的是 Mgr04.class对象
    public synchronized static Mgr04 getInstance() {
        if (INSTANCE == null) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            INSTANCE = new Mgr04();
        }

        return INSTANCE;
    }

    public void m() {
        System.out.println("m");
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                // 因为多个线程互斥调用 getInstance方法，所以线程安全，hashcode值都一样
                System.out.println(Mgr04.getInstance().hashCode());
            }).start();
        }
    }
}
