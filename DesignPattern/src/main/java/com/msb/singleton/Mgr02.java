package com.msb.singleton;

/**
 * 饿汉式
 * 使用静态代码块完成实例化
 */
public class Mgr02 {
    private static final Mgr02 INSTANCE;

    static {
        INSTANCE = new Mgr02();
    }

    private Mgr02() {
    }

    public static Mgr02 getInstance() {
        return INSTANCE;
    }

    public static void main(String[] args) {
        Mgr02 m1 = Mgr02.getInstance();
        Mgr02 m2 = Mgr02.getInstance();
        // true，m1和 m2也是同一个对象
        System.out.println(m1 == m2);
    }
}
