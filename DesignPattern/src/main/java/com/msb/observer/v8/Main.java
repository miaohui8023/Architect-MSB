package com.msb.observer.v8;


import java.util.ArrayList;
import java.util.List;

/**
 * 事件也可以形成继承体系
 */
public class Main {
    public static void main(String[] args) {
        Child child = new Child();
        child.wakeUp();
    }
}

class Child {
    private boolean cry = false;
    private List<Observer> observers = new ArrayList<>();

    {
        observers.add(new Dad());
        observers.add(new Mum());
        observers.add(new Dog());
        observers.add(e -> {
            System.out.println("ppp");
        });
    }

    public boolean isCry() {
        return cry;
    }

    public void wakeUp() {
        cry = true;
        wakeUpEvent event = new wakeUpEvent(System.currentTimeMillis(), "bed", this);

        for (Observer observer : observers) {
            observer.actionOnWakeUp(event);
        }
    }
}

abstract class Event<T> {
    abstract T getSource();
}

class wakeUpEvent extends Event<Child> {
    long timestamp;
    String loc;
    Child sorce;

    public wakeUpEvent(long timestamp, String loc, Child sorce) {
        this.timestamp = timestamp;
        this.loc = loc;
        this.sorce = sorce;
    }

    @Override
    Child getSource() {
        return sorce;
    }
}

interface Observer {
    void actionOnWakeUp(wakeUpEvent event);
}

class Dad implements Observer {

    public void feed() {
        System.out.println("dad feeding ... ~");
    }

    @Override
    public void actionOnWakeUp(wakeUpEvent event) {
        feed();
    }
}

class Mum implements Observer {

    public void hug() {
        System.out.println("mum hugging ... ~");
    }

    @Override
    public void actionOnWakeUp(wakeUpEvent event) {
        hug();
    }
}

class Dog implements Observer {
    public void bark() {
        System.out.println("dog barking ... ~");
    }

    @Override
    public void actionOnWakeUp(wakeUpEvent event) {
        bark();
    }
}