package com.msb.observer.v6;

import java.util.ArrayList;
import java.util.List;

/**
 * 很多时候，观察者需要根据时间的具体情况来进行处理
 */
public class Main {
}

class Child {
    private boolean cry = false;
    private List<Observer> observers = new ArrayList<>();

    {
        observers.add(new Dad());
        observers.add(new Mum());
        observers.add(new Dog());
    }

    public boolean isCry() {
        return cry;
    }

    public void wakeUp() {
        cry = true;
        wakeUpEvent event = new wakeUpEvent(System.currentTimeMillis(), "bed");
        for (Observer observer : observers) {
            observer.actionOnWakeUp(event);
        }
    }
}

/**
 * 事件类
 */
class wakeUpEvent {
    long timestamp;
    String loc;

    public wakeUpEvent(long timestamp, String loc) {
        this.timestamp = timestamp;
        this.loc = loc;
    }
}

interface Observer {
    void actionOnWakeUp(wakeUpEvent event);
}

class Dog implements Observer {

    public void bark() {
        System.out.println("dog barking ... ~");
    }

    @Override
    public void actionOnWakeUp(wakeUpEvent event) {
        bark();
    }
}


class Dad implements Observer {

    public void feed() {
        System.out.println("Dad feeding ... ~");
    }

    @Override
    public void actionOnWakeUp(wakeUpEvent event) {
        feed();
    }
}

class Mum implements Observer {

    public void hug() {
        System.out.println("Mum hugging ... ~");
    }

    @Override
    public void actionOnWakeUp(wakeUpEvent event) {
        hug();
    }
}