package com.msb.observer.v9;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        Button b = new Button();
    }

    private static class Button {
        private List<ActionListener> actionListeners = new ArrayList<>();

        public void buttonPressed() {
            ActionEvent e = new ActionEvent(System.currentTimeMillis(), this);
            for (int i = 0; i < actionListeners.size(); i++) {
                ActionListener al = actionListeners.get(i);
                al.actionPerformed(e);
            }
        }

        public void addActionListener(ActionListener al) {
            actionListeners.add(al);
        }
    }
}


interface ActionListener {
    void actionPerformed(ActionEvent e);
}

class MyActionListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("button pressed .. ~");
    }
}

class MyActionListener2 implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("button pressed .. 2 ~");
    }
}

class ActionEvent {
    long when;
    Object source;

    public ActionEvent(long when, Object source) {
        this.when = when;
        this.source = source;
    }

    public long getWhen() {
        return when;
    }

    public Object getSource() {
        return source;
    }
}