package com.msb.observer.v4;

/**
 * 加入多个观察者
 */
public class Main {
    public static void main(String[] args) {
        Child child = new Child();
        child.wakeUp();
    }
}

class Child {
    private boolean cry = false;
    private Dad dad = new Dad();
    private Mum mum = new Mum();
    private Dog dog = new Dog();

    public boolean isCry() {
        return cry;
    }

    public void wakeUp() {
        cry = true;
        dad.feed();
        dog.bark();
        mum.hug();
    }
}

class Dad {
    public void feed() {
        System.out.println("Dad feeding ... ~");
    }
}

class Dog {
    public void bark() {
        System.out.println("Dog barking ... ~");
    }
}

class Mum {
    public void hug() {
        System.out.println("Mum hugging ... ~");
    }
}
