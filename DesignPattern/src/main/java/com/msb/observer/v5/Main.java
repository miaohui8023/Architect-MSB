package com.msb.observer.v5;


import java.util.ArrayList;
import java.util.List;

/**
 * 分离观察者和被观察者
 */
public class Main {
    public static void main(String[] args) {
        Child child = new Child();
        child.wakeUp();
    }
}

class Child {
    private boolean cry = false;
    private List<Observer> observers = new ArrayList<>();

    {
        observers.add(new Dad());
        observers.add(new Mum());
        observers.add(new Dog());
    }

    public boolean isCry() {
        return cry;
    }

    public void wakeUp() {
        cry = true;
        for (Observer observer : observers) {
            observer.actionOnWakeUp();
        }
    }
}

interface Observer {
    void actionOnWakeUp();
}

class Dad implements Observer {

    @Override
    public void actionOnWakeUp() {
        System.out.println("Dad feeding ... ~");
    }
}

class Mum implements Observer {

    @Override
    public void actionOnWakeUp() {
        System.out.println("Mum hugging ... ~");
    }
}

class Dog implements Observer {

    @Override
    public void actionOnWakeUp() {
        System.out.println("Dog barking ... ~");
    }
}