package com.msb.observer.v7;


import java.util.ArrayList;
import java.util.List;

/**
 * 有很多时候，观察者需要根据事件的具体情况来进行处理
 * 大多数的时候，我们处理事件，需要事件源对象
 */
public class Main {
    public static void main(String[] args) {
        Child child = new Child();
        child.wakeUp();
    }
}

class Child {
    private boolean cry = false;
    private List<Observer> observers = new ArrayList<>();

    {
        observers.add(new Dad());
        observers.add(new Mum());
        observers.add(new Dog());
    }

    public boolean isCry() {
        return cry;
    }

    public void wakeUp() {
        cry = true;
        wakeUpEvent event = new wakeUpEvent(System.currentTimeMillis(), "bed", this);

        for (Observer observer : observers) {
            observer.actionOnWakeUp(event);
        }
    }
}

class wakeUpEvent {
    long timestamp;
    String loc;
    Child sorce;

    public wakeUpEvent(long timestamp, String loc, Child sorce) {
        this.timestamp = timestamp;
        this.loc = loc;
        this.sorce = sorce;
    }

}

interface Observer {
    void actionOnWakeUp(wakeUpEvent event);
}

class Dad implements Observer {

    public void feed() {
        System.out.println("dad feeding ... ~");
    }

    @Override
    public void actionOnWakeUp(wakeUpEvent event) {
        feed();
    }
}

class Mum implements Observer {

    public void hug() {
        System.out.println("mum hugging ... ~");
    }

    @Override
    public void actionOnWakeUp(wakeUpEvent event) {
        hug();
    }
}

class Dog implements Observer {
    public void bark() {
        System.out.println("dog barking ... ~");
    }

    @Override
    public void actionOnWakeUp(wakeUpEvent event) {
        bark();
    }
}