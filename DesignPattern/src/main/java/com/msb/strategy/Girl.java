package com.msb.strategy;

import java.util.Comparator;

public class Girl {

    int xiongwei, tunwei;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Girl{");
        sb.append("xiongwei=").append(xiongwei);
        sb.append(", tunwei=").append(tunwei);
        sb.append('}');
        return sb.toString();
    }

    public Girl(int xiongwei, int tunwei) {
        this.xiongwei = xiongwei;
        this.tunwei = tunwei;
    }

}
