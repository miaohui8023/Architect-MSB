package com.msb.strategy;

import java.util.Comparator;

public class Sorter {


    public static void sort(Comparable[] a) {
        for (int i = 0; i < a.length - 1; i++) {
            int miniPos = i;
            for (int j = i + 1; j < a.length; j++) {
                miniPos = a[j].compareTo(a[miniPos]) < 0 ? j : miniPos;
            }
            swap(a, i, miniPos);
        }
    }

    public static <T> void sort(T[] a, Comparator<T> comparator) {
        for (int i = 0; i < a.length - 1; i++) {
            int miniPos = i;
            for (int j = i + 1; j < a.length; j++) {
                miniPos = comparator.compare(a[j], a[miniPos]) < 0 ? j : miniPos;
            }
            swap(a, i, miniPos);
        }
    }

    public static <T> void swap(T[] a, int i, int j) {
        T temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static void swap(Comparable[] a, int i, int j) {
        Comparable temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
}
