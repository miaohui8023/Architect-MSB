package com.msb.strategy;

public class Dog implements Comparable<Dog> {
    int weight, height;

    public Dog(int weight, int height) {
        this.weight = weight;
        this.height = height;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Dog{");
        sb.append("weight=").append(weight);
        sb.append(", height=").append(height);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(Dog c) {
        int i = this.weight - c.weight;
        int j = i == 0 ? this.height - c.height : i;
        return j;
    }
}
