package com.msb.strategy;

import java.util.Arrays;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        /*int[] a = {9, 2, 3, 10, 7, 5};
        Sorter.sort(a);
        System.out.println(Arrays.toString(a));*/


        Cat[] c = {new Cat(1, 1), new Cat(3, 2), new Cat(2, 3)};
        Sorter.sort(c);
        System.out.println(Arrays.toString(c));


        Dog[] d = {new Dog(1, 1), new Dog(3, 2), new Dog(2, 3)};
        Sorter.sort(d);
        System.out.println(Arrays.toString(d));


        Girl[] g = {new Girl(100, 100), new Girl(120, 120), new Girl(110, 110)};
        Sorter.sort(g, new Comparator<Girl>() {
            @Override
            public int compare(Girl g1, Girl g2) {
                int i = g1.xiongwei - g2.xiongwei;
                int j = i == 0 ? g1.tunwei - g2.tunwei : i;
                return j;
            }
        });

        Sorter.sort(g, (g1, g2) -> {
            int i = g1.xiongwei - g2.xiongwei;
            int j = i == 0 ? g1.tunwei - g2.tunwei : i;
            return j;
        });
        System.out.println(Arrays.toString(g));
    }


}
