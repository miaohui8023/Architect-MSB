package com.msb.strategy;

public class Cat implements Comparable<Cat>{
    int weight, height;

    public Cat(int weight, int height) {
        this.weight = weight;
        this.height = height;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Cat{");
        sb.append("weight=").append(weight);
        sb.append(", height=").append(height);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(Cat c) {
        int i = this.weight - c.weight;
        int j = i == 0 ? this.height - c.height : i;
        return j;
    }
}
