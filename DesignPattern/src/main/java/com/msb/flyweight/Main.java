package com.msb.flyweight;

public class Main {
    public static void main(String[] args) {
        BulletPool bp = new BulletPool();
        for (int i = 0; i < 10; i++) {
            System.out.println(bp.getBullet());
        }
        System.out.println("----------------------------");

        String s1 = "abc";
        String s2 = "abc";
        String s3 = new String("abc");
        String s4 = new String("abc");

        System.out.println(s1 == s2); // true
        System.out.println(s1 == s3); // false
        System.out.println(s3 == s4); // false
        System.out.println(s3.intern() == s1); // true，intern()方法是 java native方法
        System.out.println(s3.intern() == s4.intern()); //true
    }
}
