package com.msb.flyweight;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BulletPool {

    List<Bullet> bullets = new ArrayList<>();

    {
        for (int i = 0; i < 5; i++) {
            bullets.add(new Bullet());
        }
    }

    public Bullet getBullet() {
        for (int i = 0; i < bullets.size(); i++) {
            Bullet b = bullets.get(i);
            if (!b.living) {
                return b;
            }
        }
        return new Bullet();
    }
}

class Bullet {
    public UUID id = UUID.randomUUID();
    boolean living = true;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Bullet{");
        sb.append("id=").append(id);
        sb.append(", living=").append(living);
        sb.append('}');
        return sb.toString();
    }
}
