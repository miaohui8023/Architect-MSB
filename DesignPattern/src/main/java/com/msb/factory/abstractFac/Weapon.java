package com.msb.factory.abstractFac;

public abstract class Weapon {
    abstract void shoot();
}
