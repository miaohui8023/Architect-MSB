package com.msb.factory.abstractFac;

public class Main {
    public static void main(String[] args) {
        System.out.println("创建一位司机");
        Car c = new Car();
        c.go();

        AK47 w = new AK47();
        w.shoot();

        Bread b = new Bread();
        b.printName();
        System.out.println("-------------------------------");

        System.out.println("定义一位巫师");
        Broom b1 = new Broom();
        b1.go();

        MagicStick m = new MagicStick();
        m.shoot();

        Mushroom mush = new Mushroom();
        mush.printName();
        System.out.println("-------------------------------");


        AbstractFactory modenFac = new ModenFactory();
        Vechile v1 = modenFac.createVechile();
        Food f1 = modenFac.createFood();
        Weapon w1 = modenFac.createWeapon();
        System.out.println("------------------------------------");

        AbstractFactory magicFac = new MagicFactory();
        Vechile v2 = magicFac.createVechile();
        Food f2 = magicFac.createFood();
        Weapon w2 = magicFac.createWeapon();

    }
}
