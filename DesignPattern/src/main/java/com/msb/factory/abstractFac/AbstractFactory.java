package com.msb.factory.abstractFac;

/**
 * 抽象工厂
 */
public abstract class AbstractFactory {
    abstract Food createFood();

    abstract Vechile createVechile();

    abstract Weapon createWeapon();
}
