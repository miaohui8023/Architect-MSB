package com.msb.factory.abstractFac;

public class ModenFactory extends AbstractFactory {
    @Override
    Food createFood() {
        return new Bread();
    }

    @Override
    Vechile createVechile() {
        return new Car();
    }

    @Override
    Weapon createWeapon() {
        return new AK47();
    }
}
