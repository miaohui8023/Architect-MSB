package com.msb.factory.simpleFac;

public class Main {
    public static void main(String[] args) {
        Car c = new Car();
        c.go();

        Plane p = new Plane();
        p.go();

        System.out.println("------------------------------");

        Vehicle v1 = new Car();
        v1.go();

        Vehicle v2 = new Plane();
        v2.go();

        Vehicle v3 = new Broom();
        v3.go();

        System.out.println("-----------------------------------");

        Vehicle c1 = new CarFactory().create();
        c1.go();

    }
}
