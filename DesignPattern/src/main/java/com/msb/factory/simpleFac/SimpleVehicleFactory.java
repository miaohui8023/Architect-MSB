package com.msb.factory.simpleFac;

/**
 * 简单工厂
 * 扩展性并不好
 */
public class SimpleVehicleFactory {

    public Car createCar() {
        // before processing
        return new Car();
    }

    public Broom createBroom() {
        return new Broom();
    }
}
