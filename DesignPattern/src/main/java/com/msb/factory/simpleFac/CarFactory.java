package com.msb.factory.simpleFac;

public class CarFactory {
    public Vehicle create() {
        System.out.println("a car has been created");
        return new Car();
    }
}
