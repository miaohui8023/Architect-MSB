package com.msb.factory.simpleFac;

public class Broom implements Vehicle {
    @Override
    public void go() {
        System.out.println("Broom riding ... ~");
    }
}
