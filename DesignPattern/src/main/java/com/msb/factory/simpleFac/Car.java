package com.msb.factory.simpleFac;

public class Car implements Vehicle {
    @Override
    public void go() {
        System.out.println("Car driving ... ~");
    }
}
