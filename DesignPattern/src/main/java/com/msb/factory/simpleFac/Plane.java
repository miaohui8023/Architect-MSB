package com.msb.factory.simpleFac;

public class Plane implements Vehicle {
    @Override
    public void go() {
        System.out.println("Plane flying ... ~");
    }
}
