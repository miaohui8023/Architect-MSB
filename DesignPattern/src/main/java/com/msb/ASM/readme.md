# ASM框架

[ASM官网 https://asm.ow2.io/](https://asm.ow2.io/)

## 介绍

对 Java字节码的操作框架<br>
[文档下载地址https://asm.ow2.io/asm4-guide.pdf](https://asm.ow2.io/asm4-guide.pdf)

>the ASM name does not mean anything: it is just a reference to the __asm__ keyword in
>C, which allows some functions to be implemented in assembly language.


