package com.msb.visitor;

public class Main {
    public static void main(String[] args) {
        PersonelVisitor v = new PersonelVisitor();
        new Computer().accept(v);
        System.out.println(v.totalPrice);
    }
}
