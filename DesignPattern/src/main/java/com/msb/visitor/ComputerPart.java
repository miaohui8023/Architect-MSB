package com.msb.visitor;

abstract public class ComputerPart {
    abstract void accept(Visitor v);
    abstract double getPrice();
}
