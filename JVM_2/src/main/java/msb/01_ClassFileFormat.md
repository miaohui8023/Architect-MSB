# JVM

## 1：JVM基础知识

1. 什么是JVM
2. 常见的JVM

## 2：ClassFileFormat



## 3：类编译-加载-初始化

- 加载过程
    - Loading：将字节码文件加载到内存
    - Linking
        - Verification：校验 CAFE BABE
        - Preparation：将静态变量赋默认值
        - Resolution：解析
    - Initializing：将静态变量赋初始值

- 类加载器：JVM按需动态加载，采用双亲委派机制
    - BootStrap：加载 lib/rt.jar charset.jar等核心类，C++实现
    - Extention/Platform：加载扩展 jar包，jre/lib/*.jar, 或由 -Djava.ext.dirs指定
    - Application：加载 classpath指定内容
    - Custom：自定义 ClassLoader
    - 一个 class文件在被加载时，会自底向上检查，查看该类是否已被加载；如果没有，就自顶向下进行实际查找和加载；如果无法加载，就抛出 ClassNotFoundException异常
    - 采用双亲委派的原因：
        - 安全
        - 已加载了的字节码文件不必再加载
        - 不会发生 class覆盖现象
        - 父加载器不是“类加载器的加载器”也不是“继承关系的加载器”
        - 双亲委派是一个孩子向着父亲方向搜索，然后父亲向着孩子方向加载的过程
- 懒加载（lazyloading其实应该叫 lazyInitializing）
    - JVM并没有规定何时加载
    - 但是严格规定了什么时候必须初始化
        - new getstatic putstatic invokestatic指令，访问 final变量除外（解析时已经固定）
        - java.lang.reflect对类进行反射调用
        - 初始化子类的时候，父类首先初始化
        - 虚拟机启动的时候，被执行的主类必须初始化
        - 动态语言支持 lava.lang.invoke.MethodHandle解析的结果为
          REF_getstatic REF_putstatic REF_invokestatic的方法句柄，该类必须初始化
- 自定义类加载器
    - extends ClassLoader
    - overwrite findClass() -> defineClass(byte[] -> Class clazz)
    - 加密
- 解释器：bytecode interpreter
- JIT：Just In Time compiler
- 混合模式
    - 混合使用 解释器 + 热点代码编译
    - 起始阶段采用解释执行
    - 热点代码检测
        - 多次被调用的方法（方法计数器：检测方法执行频率）
        - 多次被调用的循环（循环计数器：检测循环执行频率）
        - -Xmixed 默认为混合模式，开始解释执行，启动速度快
        - -Xint 使用纯解释模式，启动很快，执行稍慢
        - -Xcomp 使用纯编译模式，启动很慢，执行很快
    - 检测方式：-Xx:CompileThreshold = 10000

hashcode
锁的信息（2位 四种组合）
GC信息（年龄）
如果是数组，数组的长度

## 4：JMM

new Cat()
pointer -> Cat.class
寻找方法的信息

## 5：对象

1：句柄池 （指针池）间接指针，节省内存
2：直接指针，访问速度快

## 6：GC基础知识

栈上分配
TLAB（Thread Local Allocation Buffer）
Old
Eden
老不死 - > Old

## 7：GC常用垃圾回收器

new Object()
markword          8个字节
类型指针           8个字节
实例变量           0
补齐                  0		
16字节（压缩 非压缩）
Object o
8个字节
JVM参数指定压缩或非压缩

