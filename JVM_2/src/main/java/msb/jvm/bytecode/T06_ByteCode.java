package msb.jvm.bytecode;

public class T06_ByteCode {
    int i = 0;
    String s = "Hello, ByteCode~";

    public T06_ByteCode(int i, String s) {
        this.i = i;
        this.s = s;
    }

    public void m() {
    }
}
