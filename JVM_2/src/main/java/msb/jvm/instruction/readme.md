# 常用指令

## store

## load

## invoke

- InvokeStatic
- InvokeVirtual
- InvokeInterface
- InvokeSpecial
    - 可以直接定位，不是多态的方法
- InvokeDynamic

## pop

## mul

## sub