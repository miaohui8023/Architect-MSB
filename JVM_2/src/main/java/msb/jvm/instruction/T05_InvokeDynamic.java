package msb.jvm.instruction;

import msb.jvm.basic.T;

public class T05_InvokeDynamic {
    public static void main(String[] args) {
        I i = C::n;
        I i2 = C::n;
        I i3 = C::n;

        System.out.println(i.getClass());
        System.out.println(i2.getClass());
        System.out.println(i3.getClass());

    }

    public static interface I {
        void m();
    }

    public static class C {
        static void n() {
            System.out.println("hello");
        }
    }
}
