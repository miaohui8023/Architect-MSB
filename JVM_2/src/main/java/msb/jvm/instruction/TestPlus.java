package msb.jvm.instruction;

public class TestPlus {
    public static void main(String[] args) {
        int i = 8;
        // i = i++; // 8
        i = ++i; // 9
        System.out.println(i);
    }
}
