package msb.jvm.instruction;

/**
 * 查看 jclasslib
 * 1:53:44
 */
public class Hello03_recursion {
    public static void main(String[] args) {
        Hello03_recursion h = new Hello03_recursion();
        int i = h.m1(5);
    }

    int m1(int n) {
        if (n == 1) {
            return 1;
        }

        return n * m1(n - 1);
    }
}
