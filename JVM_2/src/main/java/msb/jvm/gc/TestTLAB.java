package msb.jvm.gc;

/**
 * -XX:-DoEscapeAnalysis -XX:-EliminateAllocations -XX:-UseTLAB -Xlog:c5_gc*
 * 逃逸分析、标量替换、线程专有对象分配
 * 强制不允许在栈上分配，只能在 Eden区中分配
 * <p>
 * 线程本地分配 TLAB(Thread Local Allocation Buffer)
 * 占用 Eden，默认1%
 * 多线程的时候不用竞争 Eden就可以申请空间，提高效率
 * 适合小对象
 * 1:06:12
 */
public class TestTLAB {
    class User {
        int id;
        String name;

        public User(int id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    void alloc(int i) {
        new User(i, "name " + i);
    }

    public static void main(String[] args) {
        TestTLAB t = new TestTLAB();
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000_000; i++) {
            t.alloc(i);
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }
}
