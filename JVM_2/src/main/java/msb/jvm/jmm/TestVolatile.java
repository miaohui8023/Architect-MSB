package msb.jvm.jmm;

public class TestVolatile {

    volatile int j;
    int i;

    public static void main(String[] args) {
        TestVolatile tv = new TestVolatile();
        tv.j = 1;
        tv.i = 1;
    }
}
