package msb.jvm.classloader;

/**
 * ClassLoader源码解析
 * findClass：如果是 AppClassLoader首先会执行 URLClassLoader的 findClass()方法
 * 采用 模板设计模式
 */
public class T05_LoadClassByHand {
    public static void main(String[] args) throws ClassNotFoundException {
        Class clazz = T05_LoadClassByHand.class.getClassLoader().loadClass("msb.jvm.classloader.T02_ClassLoaderLevel");
        System.out.println(clazz.getName());
    }
}
