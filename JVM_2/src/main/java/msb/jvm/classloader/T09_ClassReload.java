package msb.jvm.classloader;

public class T09_ClassReload {
    public static void main(String[] args) throws ClassNotFoundException {
        T06_MSBClassLoader msbClassLoader = new T06_MSBClassLoader();
        Class clazz = msbClassLoader.loadClass("msb.jvm.classloader.Test");

        msbClassLoader = null;
        System.out.println(clazz.hashCode());

        msbClassLoader = null;
        msbClassLoader = new T06_MSBClassLoader();
        Class clazz1 = msbClassLoader.loadClass("msb.jvm.classloader.Test");
        System.out.println(clazz1.hashCode());

        System.out.println(clazz == clazz1);
    }
}
