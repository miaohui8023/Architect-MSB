package msb.jvm.classloader;


public class T01_ClassLoadingProcedure {
    public static void main(String[] args) {
        System.out.println(Ts.count);
    }

}

class Ts {
    /*
    首先进行 Preparation，静态变量赋默认值，t为null，count为0
    然后进行 Initialization，先实例化 Ts，调用构造方法 count++以后，count变成了 1
    最后再给 count赋初值
     */
    public static Ts t = new Ts();
    public static int count = 2;

    private Ts() {
        count++;
    }
}
