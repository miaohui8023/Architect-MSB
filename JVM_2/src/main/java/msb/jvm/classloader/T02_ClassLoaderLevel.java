package msb.jvm.classloader;

public class T02_ClassLoaderLevel {
    public static void main(String[] args) {
        System.out.println(String.class.getClassLoader());
        System.out.println(sun.misc.Signal.class.getClassLoader());
        System.out.println(sun.reflect.ReflectionFactory.class.getClassLoader());
        System.out.println(T02_ClassLoaderLevel.class.getClassLoader());
        System.out.println("--------------------------------------------");

        System.out.println(T02_ClassLoaderLevel.class.getClassLoader().getClass().getClassLoader());
        System.out.println("---------------------------------------------");

        System.out.println(ClassLoader.getSystemClassLoader());

    }
}
