package msb.jvm.classloader;

public class T03_ClassLoaderScope {
    public static void main(String[] args) {
        String pathApp = System.getProperty("java.class.path");
        System.out.println(pathApp.replaceAll(";", System.lineSeparator()));
        System.out.println("-----------------------------------------");
    }
}
