package msb.jvm.classloader;

import java.io.*;

public class T09_ClassReload_1 {
    private static class MyLoader extends ClassLoader {
        @Override
        public Class<?> loadClass(String name) throws ClassNotFoundException {
            File f = new File("F:/idea/Architect-MSB/JVM_2/src/main/java/msb/" + name.replace(".", "/").concat(".class"));
            if (!f.exists()) {
                return super.loadClass(name);
            }

            try {
                InputStream is = new FileInputStream(f);
                byte[] b = new byte[is.available()];
                is.read(b);
                return defineClass(name, b, 0, b.length);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return super.loadClass(name);
        }
    }

    public static void main(String[] args) throws ClassNotFoundException {
        MyLoader m = new MyLoader();
        Class clazz = m.loadClass("msb.jvm.classloader.Test");

        m = new MyLoader();
        Class clazz1 = m.loadClass("msb.jvm.classloader.Test");
        System.out.println(clazz1 == clazz);
    }
}
