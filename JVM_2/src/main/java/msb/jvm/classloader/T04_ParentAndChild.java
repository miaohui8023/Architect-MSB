package msb.jvm.classloader;

public class T04_ParentAndChild {
    public static void main(String[] args) {
        // Application
        System.out.println(T04_ParentAndChild.class.getClassLoader());
        // BootStrap
        System.out.println(T04_ParentAndChild.class.getClassLoader().getClass().getClassLoader());
        // Platform
        System.out.println(T04_ParentAndChild.class.getClassLoader().getParent());
        // BootStrap
        System.out.println(T04_ParentAndChild.class.getClassLoader().getParent().getParent());

    }
}
