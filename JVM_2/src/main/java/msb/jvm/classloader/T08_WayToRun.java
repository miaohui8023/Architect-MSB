package msb.jvm.classloader;

public class T08_WayToRun {
    public static void main(String[] args) {
        for (int i = 0; i < 100_000; i++) {
            m();
        }

        long start = System.currentTimeMillis();
        for (int i = 0; i < 100_000; i++) {
            m();
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    public static void m() {
        for (int i = 0; i < 100_000; i++) {
            long j = i % 3;
        }
    }
}
